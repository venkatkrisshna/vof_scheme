! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module basis
  use variables
  use polynomial
  implicit none

end module basis


!================================!
!   Initialize Basis Functions   !
!================================!
subroutine basis_init
  use basis
  use polynomial
  use io
  implicit none

  integer :: i

  ! Calculation of basis degrees of freedom
  ndof = int(factorial(ndims+order)/(factorial(ndims)*factorial(order)))
  ndof_nu = int(factorial(ndims+order_nu)/(factorial(ndims)*factorial(order_nu)))

  ! Allocate matrices
  allocate(Dklmt(1:max(ndof,ndof_nu),1:max(ndof,ndof_nu),1:max(ndof,ndof_nu),1:max(ndof,ndof_nu)))
  allocate(M(1:max(ndof,ndof_nu),1:max(ndof,ndof_nu),1:max(ndof,ndof_nu)))
  allocate(Var(max(ndof,ndof_nu)))
  allocate(Bas(max(ndof,ndof_nu)))

  ! Allocate basis
  allocate(  phi(0:max(order,order_nu)+1))
  allocate(basisComb(max(ndof,ndof_nu),ndims))

  do i=0,max(order,order_nu)+1
     nullify(  phi(i)%a);nullify(  phi(i)%p)
  end do

  ! Initialize matrices
  Dklmt = 0.0_WP; M = 0.0_WP; Var = 0.0_WP

  ! Generate ndims basis combinations info
  call basis_combinations

  ! Generate scaled Legendre polynomials
  call basis_legendre 

  ! Calculate mass matrix
  call basis_matrix

  return
end subroutine basis_init


!===============================================!
!          Generate basis combinations          !         
!===============================================!
subroutine basis_combinations
  use basis
  use grid
  implicit none

  integer :: or, count

  ! Initialize array
  basisComb = 0

  count=1
  do or=0,max(order,order_nu)
     call basis_combinations_calc(count,or,1)
  end do

  ! if (rank.eq.root) then
  !    print *, 'ndof=',ndof
  !    print *, 'basisComb='
  !    do or=1,max(ndof,ndof_nu)
  !       write(*,'(10I3)') basisComb(or,:)
  !    end do
  ! end if

end subroutine basis_combinations

recursive subroutine basis_combinations_calc(count,or,dim)
  use basis
  implicit none

  ! Inputs
  integer :: count
  integer, intent(in) :: or, dim

  ! Variables
  integer :: n

  if (dim.gt.ndims) return

  do n=max(or-sum(basisComb(count,1:dim-1)),0),0,-1
     basisComb(count:max(ndof,ndof_nu),dim)=n
     if (dim.eq.ndims) then
        count=count+1
        return
     end if
     call basis_combinations_calc(count,or,dim+1)
  end do

  return
end subroutine basis_combinations_calc


!=================================================!
!          Generate Legendre Polynomials          !
!=================================================!
subroutine basis_legendre
  use basis
  use polynomial
  use grid
  implicit none

  integer :: l,mm,ni
  real(QP) :: tb1,tb2,tb3

  ! Set zeta window
  zetamin = -1.0_WP
  zetamax = +1.0_WP

  ! Create a string of Legendre Polynomials
  ! Loop over n and m
  do ni = 0,max(order,order_nu)+1
     ! Allocate the string for the number of terms
     l = floor(real(ni,WP)/2.0_WP)
     phi(ni)%n = l+1
     allocate(phi(ni)%a(phi(ni)%n))
     allocate(phi(ni)%p(phi(ni)%n))
     ! Populate the poly array
     do mm = 0,l
        ! phi(ni)%a(mm+1) = (real(((-1)**mm),QP)*factorial(2*ni-2*mm))/ &
        !      (factorial(mm)*factorial(ni-mm)*factorial(ni-2*mm)*real((2**ni),QP))
        phi(ni)%p(mm+1) = (ni-2*mm)
        ! Break up the multiplication
        tb1 = (real(((-1)**mm),QP)*factorial(2*ni-2*mm))/factorial(mm)
        tb2 = tb1/factorial(ni-mm)
        tb3 = tb2/factorial(ni-2*mm)
        phi(ni)%a(mm+1) = tb3/real((2**ni),QP)
     end do
     ! if (rank.eq.root) then
     !    print *,'===========',ni,'th Basis function ==========='
     !    do mm=1,phi(ni)%n
     !       print *,real(phi(ni)%a(mm),SP),'x^',phi(ni)%p(mm)
     !    end do
     ! end if
  end do

  return
end subroutine basis_legendre

!=======================================!
!    Compile Multiplication Tensors     !      
!=======================================!
subroutine basis_matrix
  use basis
  use polynomial
  use variables
  use grid
  implicit none
  integer :: tt,aa,k,bb,l,cc,mm,dd
  real(QP), dimension(1:max(ndof,ndof_nu),1:max(ndof,ndof_nu),1:max(ndof,ndof_nu),1:max(ndof,ndof_nu)) :: Dcreate
  real(QP), dimension(1:max(ndof,ndof_nu),1:max(ndof,ndof_nu),1:max(ndof,ndof_nu)) :: Mcreate
  real(QP), dimension(max(ndof,ndof_nu)) :: Varcreate, Bascreate

  ! Calculate the Denominator
  Varcreate = 0.0_QP
  do tt = 0,max(order,order_nu)
     Varcreate(tt+1) = 2.0_QP/(2.0_QP*real(tt,QP)+1.0_QP)
  end do

  ! Compile the multiplication tensors
  ! Likely best to break up and divide by exponents first then multiply each piece together
  Dcreate = 0.0_QP; Mcreate = 0.0_QP; Bascreate = 0.0_QP
  do tt=0,order
     do aa=1,phi(tt)%n
        Bascreate(tt+1) = Bascreate(tt+1) + ((phi(tt)%a(aa)/(real(phi(tt)%p(aa),QP)+1.0_QP))* &
             real(zetamax,QP)**(phi(tt)%p(aa)+1)) - ((phi(tt)%a(aa)/(real(phi(tt)%p(aa),QP)+1.0_QP))* &
             real(zetamin,QP)**(phi(tt)%p(aa)+1))
        do k=0,order
           do bb=1,phi(k)%n
              do l=0,order
                 do cc=1,phi(l)%n
                    ! Calculate the multiplication tensor Mklt
                    Mcreate(k+1,l+1,tt+1) = Mcreate(k+1,l+1,tt+1) + &
                                ! Evaluate the integral at upper bound
                         ((((phi(k)%a(bb)*phi(l)%a(cc)*phi(tt)%a(aa))/ &
                         (real((phi(k)%p(bb)+phi(l)%p(cc)+phi(tt)%p(aa)),QP)+1.0_QP))* &
                         (real(zetamax,QP))**(phi(k)%p(bb)+phi(l)%p(cc)+phi(tt)%p(aa)+1)) - &
                                ! Subtract off the lower bound
                         (((phi(k)%a(bb)*phi(l)%a(cc)*phi(tt)%a(aa))/ &
                         (real((phi(k)%p(bb)+phi(l)%p(cc)+phi(tt)%p(aa)),QP)+1.0_QP))* &
                         (real(zetamin,QP))**(phi(k)%p(bb)+phi(l)%p(cc)+phi(tt)%p(aa)+1)))
                    do mm=0,order
                       do dd=1,phi(mm)%n
                          ! Calculate the multiplication tensor for the Diffusion term
                          ! Also can be used for the second part of the Compression term
                          Dcreate(k+1,l+1,mm+1,tt+1) = Dcreate(k+1,l+1,mm+1,tt+1) + &
                                ! Evaluate the integral at upper bound
                               ((((phi(k)%a(bb)*phi(l)%a(cc)*phi(mm)%a(dd)*phi(tt)%a(aa))/ &
                               (real((phi(k)%p(bb)+phi(l)%p(cc)+phi(mm)%p(dd)+phi(tt)%p(aa)),QP)+1.0_QP))* &
                               (real(zetamax,QP))**(phi(k)%p(bb)+phi(l)%p(cc)+phi(mm)%p(dd)+ &
                               phi(tt)%p(aa)+1)) - &
                                ! Subtract off the lower bound
                               (((phi(k)%a(bb)*phi(l)%a(cc)*phi(mm)%a(dd)*phi(tt)%a(aa))/ &
                               (real((phi(k)%p(bb)+phi(l)%p(cc)+phi(mm)%p(dd)+phi(tt)%p(aa)),QP)+1.0_QP))* &
                               (real(zetamin,QP))**(phi(k)%p(bb)+phi(l)%p(cc)+phi(mm)%p(dd)+ &
                               phi(tt)%p(aa)+1)))
                       end do
                    end do
                 end do
              end do
           end do
        end do
     end do
     Dcreate(:,:,:,tt+1) = Dcreate(:,:,:,tt+1)/Varcreate(tt+1)
     Mcreate(:,:,tt+1) = Mcreate(:,:,tt+1)/Varcreate(tt+1)
     Bascreate(tt+1) = Bascreate(tt+1)/Varcreate(tt+1)
  end do

  ! Save to working precision
  Var = real(Varcreate,WP)
  M = real(Mcreate,WP)
  Dklmt = real(Dcreate,WP)
  Bas = real(Bascreate,WP)

  ! Perform Error Check and Send Warning
  ! Due to precision errors with high numbers of basis functions
  if (rank.eq.root) then
     if ((ndof.gt.1).and.((maxval(abs(M)).gt.1.0_WP).or.(maxval(abs(Dklmt)).gt.1.0_WP).or. &
          (minval(abs(M)).gt.epsilon(0.0_WP)).or.(minval(abs(Dklmt)).gt.epsilon(0.0_WP)))) then
        print*,'!======================= ALERT =======================!'
        print*,'!=====================================================!'
        print*,'!==           Multiplication Tensor Issue           ==!'
        print*,'!=====================================================!'
        print*,'   All Tensor values should be in the range of [0,1]'
        print*,'   Cklmt Information'
        print*,'      Minimum Value = ',minval(Dklmt)
        print*,'      Maximum Value = ',maxval(Dklmt)
        print*,'   Cklt Information'
        print*,'      Minimum Value = ',minval(M)
        print*,'      Maximum Value = ',maxval(M)
        print*,'!=====================================================!'
        print*,'!==       Min and Max values cleaned to [0,1]       ==!'
        print*,'!==       Simulation Values May Be Inaccurate       ==!'
        print*,'!=====================================================!'
     else
        print*,'Multiplication Tensors Calculated Properly'
     end if
  end if

  ! Clean up integrals slightly greater than 1 and less than 0
  ! Suppose to clean up numerical errors that could cause instability
  where (Dklmt.gt.1.0_WP)
     Dklmt = 1.0_WP
  end where
  where (M.gt.1.0_WP)
     M = 1.0_WP
  end where

  where (Dklmt.lt.0.0_WP)
     Dklmt = 0.0_WP
  end where
  where (M.lt.0.0_WP)
     M = 0.0_WP
  end where

  ! Create reduced tensor
  call tensor_reduce

  return
end subroutine basis_matrix

!==============================================!
!          Compile Non-Zero Integrals          !
!==============================================!
subroutine tensor_reduce
  use io
  use grid
  use basis
  use variables
  use polynomial
  use communication

  implicit none
  integer :: kk,ll,mm,tt
  integer, dimension(ndof**4,4) :: Mldum, Dldum
  integer :: Mcheck, Dcheck

  ! Store non-zero values and their indices
  Mn = 0; Dn = 0;
  do tt = 1,ndof
     do mm = 1,ndof
        do ll = 1,ndof
           if (M(ll,mm,tt).gt.epsilon(0.0_WP)) then
              Mn = Mn+1
              Mldum(Mn,1) = ll
              Mldum(Mn,2) = mm
              Mldum(Mn,3) = tt
           end if
           do kk = 1,ndof
              if (Dklmt(kk,ll,mm,tt).gt.epsilon(0.0_WP)) then
                 Dn = Dn+1
                 Dldum(Dn,1) = kk
                 Dldum(Dn,2) = ll
                 Dldum(Dn,3) = mm
                 Dldum(Dn,4) = tt
              end if
           end do
        end do
     end do
  end do

  ! Allocate the final arrays
  allocate(Mlsave(Mn,3)); Mlsave = 0
  allocate(Dlsave(Dn,4)); Dlsave = 0

  ! Store saved information
  Mlsave = Mldum(1:Mn,1:3)
  Dlsave = Dldum(1:Dn,:)

  ! Check for errors between processors
  call sum_integer(Mn,Mcheck); call sum_integer(Dn,Dcheck)
  if ((Mcheck/nproc.ne.Mn).or.(Dcheck/nproc.ne.Dn)) then
     call die('Reduced Tensor Error (basis.f90)')
  end if

  return
end subroutine tensor_reduce

!====================================!
!    Calculate Gaussian Tensor       !
!====================================!
subroutine gauss_tensor
  use io
  use variables
  use polynomial
  use basis
  implicit none
  integer :: tt,aa

  ! Allocate the Array
  allocate(GaussTensor(1:ndof))

  GaussTensor = 0.0_WP
  do tt=0,order
     do aa=1,phi(tt)%n
        GaussTensor(tt+1) = GaussTensor(tt+1) + gauss_romberg(phi(tt),-1.0_WP,1.0_WP)
     end do
     GaussTensor(tt+1) = GaussTensor(tt+1)/Var(tt+1)
  end do

  return
end subroutine gauss_tensor

! !==================================================!
! !          Compile Multiplication Tensors          !      
! !==================================================!
! subroutine basis_matrix
!   use basis
!   use polynomial
!   use variables
!   use grid
!   implicit none

!   integer :: count, dir
!   integer :: j, k, l, t
!   integer :: jj, kk, ll, tt
!   type(poly) :: polytmp
!   real(QP), dimension(1:max(ndof,ndof_nu),1:max(ndof,ndof_nu),1:max(ndof,ndof_nu),1:max(ndof,ndof_nu)) :: Dcreate
!   real(QP), dimension(1:max(ndof,ndof_nu),1:max(ndof,ndof_nu),1:max(ndof,ndof_nu)) :: Mcreate
!   real(QP), dimension(max(ndof,ndof_nu)) :: Varcreate

!   ! Allocate matrices
!   Mcreate = 1.0_QP; Dcreate = 1.0_QP; Varcreate = 1.0_WP

!   do t = 1,max(ndof,ndof_nu)

!      ! Calculate denominator ========================================================= !
!      Varcreate(t) = 1.0_QP

!      ! Compute integral of phi_k * phi_k over [zeta_min,zeta_max]^ndims = [-1,1]^ndims
!      do dir = 1,ndims

!         ! Allocate polytmp
!         polytmp%n=phi(basisComb(t,dir))%n*phi(basisComb(t,dir))%n
!         allocate(polytmp%a(polytmp%n))
!         allocate(polytmp%p(polytmp%n))

!         ! Form phi_i*phi_j in direction dir in polytmp
!         count = 0
!         do tt = 1,phi(basisComb(t,dir))%n
!            count = count+1
!            polytmp%a(count) = phi(basisComb(t,dir))%a(tt)*phi(basisComb(t,dir))%a(tt)
!            polytmp%p(count) = phi(basisComb(t,dir))%p(tt)+phi(basisComb(t,dir))%p(tt)
!         end do

!         ! Compute integral
!         Varcreate(t) = Varcreate(t)*integral(polytmp,real(zetamin,QP),real(zetamax,QP))

!         ! Deallocate temporary polynomial
!         polytmp%n = 0
!         deallocate(polytmp%a)
!         deallocate(polytmp%p)        

!      end do

!      ! Calculate numerator and form matrices ========================================= !       
!      do k=1,max(ndof,ndof_nu)
!         do l=1,max(ndof,ndof_nu)

!            ! Compute integral of phi_i phi_j * phi_k over [xmin,xmax]^3
!            do dir=1,ndims

!               ! Allocate polytmp
!               polytmp%n=phi(basisComb(k,dir))%n*phi(basisComb(l,dir))%n*phi(basisComb(t,dir))%n
!               allocate(polytmp%a(polytmp%n))
!               allocate(polytmp%p(polytmp%n))

!               ! Form phi_i*phi_j*phi_k in direction dir in polytmp
!               count = 0
!               do tt=1,phi(basisComb(t,dir))%n
!                  do ll=1,phi(basisComb(l,dir))%n
!                     do kk=1,phi(basisComb(k,dir))%n
!                        count=count+1
!                        polytmp%a(count)=phi(basisComb(k,dir))%a(kk)*phi(basisComb(l,dir))%a(ll)*phi(basisComb(t,dir))%a(tt)
!                        polytmp%p(count)=phi(basisComb(k,dir))%p(kk)+phi(basisComb(l,dir))%p(ll)+phi(basisComb(t,dir))%p(tt)
!                     end do
!                  end do
!               end do

!               ! Compute integral
!               Mcreate(k,l,t) = Mcreate(k,l,t)*integral(polytmp,real(zetamin,QP),real(zetamax,QP))

!               ! Deallocate temporary polynomial
!               polytmp%n=0
!               deallocate(polytmp%a)
!               deallocate(polytmp%p)

!            end do

!            ! Compute 4th order multiplication tensor
!            do j = 1,max(ndof,ndof_nu)
!               do dir = 1,ndims

!                  ! Allocate polytmpD
!                  polytmp%n=phi(basisComb(j,dir))%n*phi(basisComb(k,dir))%n*phi(basisComb(l,dir))%n*phi(basisComb(t,dir))%n
!                  allocate(polytmp%a(polytmp%n))
!                  allocate(polytmp%p(polytmp%n))

!                  ! Form phi_i*phi_j*phi_k*phi_l in direction dir in polytmp
!                  count = 0
!                  do tt=1,phi(basisComb(t,dir))%n
!                     do ll=1,phi(basisComb(l,dir))%n
!                        do kk=1,phi(basisComb(k,dir))%n
!                           do jj=1,phi(basisComb(j,dir))%n
!                              count=count+1
!                              polytmp%a(count) = phi(basisComb(j,dir))%a(jj)* &
!                                   phi(basisComb(k,dir))%a(kk)*phi(basisComb(l,dir))%a(ll)*phi(basisComb(t,dir))%a(tt)
!                              polytmp%p(count) = phi(basisComb(j,dir))%p(jj) + &
!                                   phi(basisComb(k,dir))%p(kk)+phi(basisComb(l,dir))%p(ll)+phi(basisComb(t,dir))%p(tt)
!                           end do
!                        end do
!                     end do
!                  end do

!                  ! Compute integral
!                  Dcreate(j,k,l,t) = Dcreate(j,k,l,t)*integral(polytmp,real(zetamin,QP),real(zetamax,QP))

!                  ! Deallocate temporary polynomial
!                  polytmp%n=0
!                  deallocate(polytmp%a)
!                  deallocate(polytmp%p)

!               end do
!            end do


!         end do
!      end do

!      Mcreate(:,:,t) = Mcreate(:,:,t)/Varcreate(t)
!      Dcreate(:,:,:,t) = Dcreate(:,:,:,t)/Varcreate(t)

!   end do

!   ! Save to working precision
!   Var = real(Varcreate,WP)
!   M = real(Mcreate,WP)
!   Dklmt = real(Dcreate,WP)

!   ! Perform Error Check and Send Warning
!   ! Due to precision errors with high numbers of basis functions
!   if (rank.eq.root) then
!      if ((ndof.gt.1).and.((maxval(abs(M)).gt.1.0_WP).or.(maxval(abs(Dklmt)).gt.1.0_WP).or. &
!           (minval(abs(M)).gt.epsilon(0.0_WP)).or.(minval(abs(Dklmt)).gt.epsilon(0.0_WP)))) then
!         print*,'!======================= ALERT =======================!'
!         print*,'!=====================================================!'
!         print*,'!==           Multiplication Tensor Issue           ==!'
!         print*,'!=====================================================!'
!         print*,'   All Tensor values should be in the range of [0,1]'
!         print*,'   Cklmt Information'
!         print*,'      Minimum Value = ',minval(Dklmt)
!         print*,'      Maximum Value = ',maxval(Dklmt)
!         print*,'   Cklt Information'
!         print*,'      Minimum Value = ',minval(M)
!         print*,'      Maximum Value = ',maxval(M)
!         print*,'!=====================================================!'
!         print*,'!==       Min and Max values cleaned to [0,1]       ==!'
!         print*,'!==       Simulation Values May Be Inaccurate       ==!'
!         print*,'!=====================================================!'
!      else
!         print*,'Multiplication Tensors Calculated Properly'
!      end if
!   end if

!   ! Clean up integrals slightly greater than 1 and less than 0
!   ! Suppose to clean up numerical errors that could cause instability
!   where (Dklmt.gt.1.0_WP)
!      Dklmt = 1.0_WP
!   end where
!   where (M.gt.1.0_WP)
!      M = 1.0_WP
!   end where

!   where (Dklmt.lt.0.0_WP)
!      Dklmt = 0.0_WP
!   end where
!   where (M.lt.0.0_WP)
!      M = 0.0_WP
!   end where

!   ! Create reduced tensor
!   call tensor_reduce

!   return
! end subroutine basis_matrix

