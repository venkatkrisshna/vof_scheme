! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

!=========================!
!    Global Variables     !
!=========================!
module variables
  implicit none

  ! Precision 
  integer, parameter :: SP = kind(1.0)
  integer, parameter, private :: DP = kind(1.0d0)
  integer, parameter :: QP = kind(1.0q0)
  integer, parameter :: WP = DP

  ! String lengths
  integer, parameter :: str_short  = 8
  integer, parameter :: str_medium = 64
  integer, parameter :: str_long   = 8192

  ! Simulation
  character(len=100) :: simu

  ! Number of cells
  integer :: nx, ny

  ! Number of ghost cells on boundary
  integer :: nghost

  ! Physical size of domain
  real(WP) :: xmin, xmax, ymin, ymax

  ! Periodicity
  integer :: xper, yper
  logical :: isxper, isyper
  integer :: px,py

  ! Message passing
  integer :: Lsend, Lrecv, Rsend, Rrecv
  integer :: Bsend, Brecv, Tsend, Trecv

  ! Boundary conditions
  character(len=100) :: bc_y
  logical :: flux

  ! Iterations
  integer :: iter,niter

  ! Data Writing (to binary file)
  ! Write Frequency
  integer :: wrtfr
  ! File categorization
  character(len=100) :: filext
  logical :: restart
  real(WP) :: restart_time

  ! Simulation complete?
  logical :: isdone

  ! Using multiphase?
  logical :: use_multiphase

  !========== Basis Variables ==========!

  ! Data structure for polynomial: P(x)=sum{a(i)*x**p(i),i=1..n}
  type poly
     integer :: n
     real(QP), dimension(:), pointer :: a
     integer,  dimension(:), pointer :: p
  endtype poly

  real(WP),   dimension(:,:,:,:),     pointer :: Dklmt     ! 4th order multiplication tensor
  real(WP),   dimension(:,:,:),       pointer :: M         ! 3rd order multiplication tensor
  real(WP),   dimension(:),           pointer :: Var       ! Variance of each basis
  real(WP),   dimension(:),           pointer :: Bas       ! Integral of each basis function
  type(poly), dimension(:),           pointer :: phi       ! Basis functions (1D)
  integer,    dimension(:,:),         pointer :: basisComb ! multidim  basis combination
  integer                                     :: ndof      ! number of degrees of freedom 
  integer                                     :: ndof_nu   ! number of dof for nu

  !========== Velocity Variables ==========!

  ! Velocity field & derivatives
  real(WP), dimension(:,:,:), pointer :: u, u_old, ustar
  real(WP), dimension(:,:,:), pointer :: v, v_old, vstar
  real(WP), dimension(:,:,:), pointer :: div_ux, div_vy, div_u
  real(WP), dimension(:,:,:), pointer :: u_now, v_now
  real(WP), dimension(:,:,:), pointer :: u_bot, v_lef
  real(WP), dimension(:,:,:), pointer :: Uint, Vint
  real(WP), dimension(:,:,:), pointer :: dudx, dvdx, dudy, dvdy
  real(WP), dimension(:,:,:), pointer :: dudxdx, dudydy, dvdxdx, dvdydy
  real(WP) :: vel_divergence, div_avg

  ! Source terms
  real(WP), dimension(:,:,:), pointer :: sourceU, sourceV

  ! Right hand sides
  real(WP), dimension(:,:,:), pointer :: rhsU, rhsV
  real(WP), dimension(:,:,:), pointer :: Ucor, Vcor

  !========== Pressure Variables ==========!

  ! Pressure field
  real(WP), dimension(:,:,:), pointer :: Pr, P_rhs
  real(WP), dimension(:,:,:), pointer :: Pr_old, Pr_now
  real(WP), dimension(:,:,:), pointer :: P_hat
  real(WP), dimension(:,:,:), pointer :: dPdx, dPdy
  real(WP), dimension(:,:,:), pointer :: rhoI_denom
  real(WP), dimension(:,:,:), pointer :: Prx_p1, Prx_m1
  real(WP), dimension(:,:,:), pointer :: Pry_p1, Pry_m1
  real(WP), dimension(:,:,:), pointer :: off_Prx, off_Pry
  logical :: testPr

  ! Max pressure estimation iterations
  integer :: ctr3, pei
  real(WP) :: myPhat_res, Phat_res

  ! Max convergence iterations (elliptic)
  integer :: pressure_iterations

  ! HYPRE variables
  logical :: use_HYPRE
  character(len=100) :: pressure_solver
  character(len=100) :: pressure_precond

  ! FFTW variables
  logical :: use_FFTW

  ! Pressure testing
  real(WP), dimension(:,:,:), pointer :: P_other, P_diff, P_save
  real(WP), dimension(:,:,:), pointer :: u_diff, v_diff

  ! Viscosity
  real(WP), dimension(:,:,:), pointer :: nu
  real(WP), dimension(:,:,:), pointer :: nu_lef, nu_bot
  real(WP), dimension(:),     pointer :: nuI
  real(WP), dimension(:),     pointer :: nuE
  real(WP)                            :: nu_io
  real(WP)                            :: nu_ioE
  real(WP) :: sigmaVI
  real(WP) :: sigmaVE

  ! Density
  real(WP), dimension(:,:,:), pointer :: rhok, rhoIk
  real(WP), dimension(:,:,:), pointer :: iD_lef, iD_bot
  real(WP), dimension(:,:,:), pointer :: rho_lef, rho_bot
  real(WP), dimension(:,:,:), pointer :: drhoIdx, drhoIdy
  real(WP), dimension(:,:,:), pointer :: rhoX, rhoY
  real(WP), dimension(:,:,:), pointer :: Rco
  real(WP), dimension(:),     pointer :: rhoI, rhoE
  real(WP) :: rho_io
  real(WP) :: rho_ioE
  real(WP) :: rhoDiff
  real(WP) :: sigmaDI
  real(WP) :: sigmaDE
  real(WP) :: rho_0

  ! Surface tension coefficient
  real(WP) :: gamma
  real(WP) :: sigmaG
  real(WP), dimension(:),     pointer :: gammak

  ! Scalar
  real(WP), dimension(:,:,:), pointer :: SC

  ! Uncertainty
  real(WP), dimension(:),     pointer :: GaussTensor
  real(WP) :: zetamin, zetamax

  ! Multiplication Tensor Reduction
  integer,  dimension(:,:),   pointer :: Mlsave
  integer,  dimension(:,:),   pointer :: Dlsave
  integer :: Mn, Dn

  !============= Test Case Variables ===============!

  ! Geometry
  character(len=100) :: Gshape

  ! Circle
  real(WP) :: xo, yo, D

  ! Ellipse
  real(WP) :: semimajor, semiminor

  ! Zalesak's Disk
  real(WP), dimension(:), pointer :: Cz

  ! Level Set Test
  logical :: LST

  ! Oscillating Droplet
  real(WP), dimension(:,:,:), pointer :: KE
  real(WP), dimension(:,:),   pointer :: gKE

  ! Deformation Case
  real(WP), dimension(:,:,:), pointer :: iVol
  real(WP), dimension(:,:),   pointer :: giVol

  ! Incoming velocity
  real(WP) :: Umag, Vmag
  real(WP) :: Umag_var, Vmag_var

  !============= Level Set Transport ===============!

  ! Crank Nicholson Iterations
  integer :: ctr1,cni

  ! Maximum Reinitialization Steps
  integer :: ctr2,mrs

  ! Initial Level Set Convergence
  integer :: ilsc

  ! Romberg Integration
  real(QP), dimension(:,:), pointer :: ri
  real(WP), dimension(:,:,:,:), pointer :: riV

  ! High Order Upwind Central - Order
  integer :: nhouc

  ! HOUC Weights
  real(WP), dimension(:), pointer :: houc_xp
  real(WP), dimension(:), pointer :: houc_xm
  real(WP), dimension(:), pointer :: houc_yp
  real(WP), dimension(:), pointer :: houc_ym

  ! Conservative LS epsilon
  real(WP) :: epsG
  real(WP) :: epsG2

  ! Level set
  real(WP), dimension(:,:,:), pointer :: G
  real(WP), dimension(:,:,:), pointer :: Gxi, Gyi
  real(WP), dimension(:,:,:), pointer :: Gco
  real(WP), dimension(:,:,:), pointer :: Gold, G_now
  real(WP), dimension(:,:,:), pointer :: Gupx, Gupy
  real(WP), dimension(:,:,:), pointer :: rhsG
  real(WP), dimension(:,:,:), pointer :: Gdx, Gdy
  real(WP), dimension(:,:,:), pointer :: Gdxi, Gdyi
  real(WP), dimension(:,:,:), pointer :: Gprob, Gsum
  ! Compression
  real(WP), dimension(:,:,:), pointer :: comp
  real(WP), dimension(:,:,:), pointer :: comppi, compmi
  real(WP), dimension(:,:,:), pointer :: comppj, compmj
  ! Diffusion
  real(WP), dimension(:,:,:), pointer :: diff, diff2
  real(WP), dimension(:,:,:), pointer :: diffpi, diffmi
  real(WP), dimension(:,:,:), pointer :: diffpj, diffmj
  ! Normal variables
  real(WP), dimension(:,:,:), pointer :: Nox, Noy
  real(WP), dimension(:,:,:), pointer :: Nxi, Nyi
  real(WP), dimension(:,:,:), pointer :: Nxv
  real(WP), dimension(:,:,:), pointer :: Nyh
  real(WP) :: norm, C_norm
  real(WP) :: topx, botxi, topy, botyi
  integer :: delt

  ! Curvature
  real(WP), dimension(:,:,:), pointer :: Ks
  real(WP), dimension(:,:,:), pointer :: Kx, Ky
  real(WP), dimension(:,:,:), pointer :: Kxx, Kyy
  integer :: gridbase

  ! Quadrature
  real(WP), dimension(:,:), pointer :: Quad_bas
  real(WP), dimension(:), pointer :: Rgauss
  real(WP), dimension(:), pointer :: Wgauss
  integer :: quadpts

  ! Conservation Variables
  real(WP) :: Gi1, Gi2, AvgIntDiff

  !=============== Logo Development ==============!
  real(WP), dimension(:,:,:), pointer :: FFF
  real(WP), dimension(:,:,:), pointer :: CCC
  real(WP), dimension(:,:,:), pointer :: LLL
  real(WP), dimension(:,:,:), pointer :: Gsave

  !========== Inputs ==========!
  ! Time
  real(WP) :: time
  real(WP) :: dt, dtre
  real(WP) :: CFL, ReCFL
  real(WP) :: F_tau
  real(WP) :: dtmin
  real(WP) :: simulation_time

  ! Pressure convergence
  real(WP) :: cvg_criteria
  real(WP), dimension(:,:), pointer :: r 
  integer :: pressure_steps, pressure_max_steps
  real(WP) :: p_res

  ! Mask (marks where walls are)
  real(WP), dimension(:,:), pointer :: mask

  ! Uncertainty quantification
  logical :: is_uq        ! Run UQ
  integer :: order        ! Order of basis
  integer :: order_nu     ! Order of basis for nu
  integer :: ndims        ! Number of uncertain variables
  logical :: MCS          ! Run Monte Carlo?
  integer :: mci          ! Number of monte carlo iterations
  logical :: LSProbCalc   ! Calculate probability of level set?
  real(WP) :: sigma       ! Nu uncertainty parameter
  real(WP) :: sigmaI      ! Starting location uncertainty parameter
  real(WP), dimension(:,:), allocatable :: var_u, var_v

  ! Gravity
  logical :: gravity
  real(WP) :: grav_acc, grav_var
  real(WP), dimension(:), pointer :: Gr

end module variables


