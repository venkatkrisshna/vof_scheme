### Compiled solver name
CODE = multiUQ

### Compiler name 
F90 = mpifort

### Flags (uncomment for your compiler)
## GNU
DEBUGFLAGS = -Wall -Wextra -pedantic -g3 -fbounds-check -fbacktrace
OPTFLAGS = -O3 -ffast-math -funroll-loops -fomit-frame-pointer -pipe
### Intel ###
# DEBUGFLAGS = -O0 -g -CA -CB -CS -CV -traceback -debug all -ftrapuv -warn all
# OPTFLAGS = -O3 -xT -ip

### Library locations (modify based on where libraries are)
HDF5_DIR = /usr/local/hdf5-1.10.5
SZIP_DIR = /usr/local/szip-2.1.1
SILO_DIR = /usr/local/silo-4.10.2
FFTW_DIR = /usr/local/fftw-3.3.8
HYPRE_DIR = /usr/local/hypre-2.11.2

### Library linking
SILO_INC = -I$(SILO_DIR)/include
SILO_LIB = -L$(SILO_DIR)/lib -lsiloh5 -L$(HDF5_DIR)/lib -lhdf5 -L$(SZIP_DIR)/lib -lsz -L/usr/local/lib -lstdc++ -lz
FFTW_INC = -I$(FFTW_DIR)/include
FFTW_LIB = -L$(FFTW_DIR)/lib -lfftw3_mpi
HYPRE_INC = -I$(HYPRE_DIR)/include
HYPRE_LIB = -L$(HYPRE_DIR)/lib -lHYPRE

opt : FLAGS = $(OPTFLAGS)
debug : FLAGS = $(DEBUGFLAGS)

#### Files to compile
OBJ = variables.o io.o parallel.o grid.o polynomial.o communication.o basis.o math.o uq.o velocity.o boundary.o fourier.o pressure.o hypre.o levelset.o simulation.o timing.o visit.o geometry.o data.o main.o  

### Compiling options 
%.o: %.f90 $(DEPS)
	$(F90) $(FLAGS) -c -o $@ $< $(SILO_INC) $(FFTW_INC) $(HYPRE_INC)
default: $(OBJ)	
	$(F90) $(FLAGS) -o $(CODE) $^ $(SILO_LIB) $(FFTW_LIB) $(HYPRE_LIB)

.PHONY: clean

opt: $(OBJ)
	$(F90) $(FLAGS) -o $(CODE) $^ $(SILO_LIB) $(FFTW_LIB) $(HYPRE_LIB)

debug: $(OBJ)
	$(F90) $(FLAGS) -o $(CODE) $^ $(SILO_LIB) $(FFTW_LIB) $(HYPRE_LIB)

clean:
	rm -f *.o *.mod *~ $(CODE)

cleanup: rm -f *~
