! This file is part of multiUQ.
!
! multiUQ is free software: you can redistribute it and/or modify
! it under the terms of the GNU General Public License as published by
! the Free Software Foundation, either version 3 of the License, or
! (at your option) any later version.
!
! multiUQ is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! You should have received a copy of the GNU General Public License
! along with multiUQ.  If not, see <http://www.gnu.org/licenses/>.

module boundary
  use grid
  use variables
  use basis
  use communication
  implicit none


end module boundary

!==============================!
! Velocity boundary conditions !
!==============================!
subroutine velocity_bc
  use velocity
  implicit none

  integer :: i,j,k
  integer :: verr
  real(WP), dimension(1:ndof) :: FluxIn_local, FluxOut_local
  real(WP), dimension(1:ndof) :: FluxIn, FluxOut

  ! Update boundary conditions based on test case
  select case(trim(simu))

  case ('null','Null')
     ! No slip boundary conditions on all walls
     if (rankx.eq.0) then
        do i=1,nghost
           vstar(imin_-i,:,:) = -vstar(imin_,:,:)
        end do
        ustar(imino_:imin_,:,:) = 0.0_WP
     end if
     if (rankx.eq.px-1) then
        do i=1,nghost
           vstar(imax_+i,:,:) = -vstar(imax_,:,:)
        end do
        ustar(imax_+1:imaxo_,:,:) = 0.0_WP
     end if
     if (ranky.eq.0) then
        do j=1,nghost
           ustar(:,jmin_-j,:) = -ustar(:,jmin_,:)
        end do
        vstar(:,jmino_:jmin_,:) = 0.0_WP
     end if
     if (ranky.eq.py-1) then
        do j=1,nghost
           ustar(:,jmax_+j,:) = -ustar(:,jmax_,:)
        end do
        vstar(:,jmax_+1:jmaxo_,:) = 0.0_WP
     end if

  case ('Jet','JET','jet')
     ! Set top and bottom walls to 0
     if (ranky.eq.py-1) then
        do j=1,nghost
           ustar(:,jmax_+j,:) = -ustar(:,jmax_,:)
        end do
        vstar(:,jmax_+1:jmaxo_,:) = 0.0_WP
     end if
     if (ranky.eq.0) then
        do j=1,nghost
           ustar(:,jmin_-j,:) = -ustar(:,jmin_,:)
        end do
        vstar(:,jmino_:jmin_,:) = 0.0_WP
     end if

     ! Incoming Jet Flow
     FluxIn_local  = 0.0_WP; FluxIn  = 0.0_WP
     FluxOut_local = 0.0_WP; FluxOut = 0.0_WP
     if (rankx.eq.0) then
        ! Inflow
        ustar(imino_:imin_,:,:) = 0.0_WP
        do i=1,nghost
           vstar(imin_-i,:,:) = -vstar(imin_,:,:)
        end do
        ! Incoming jet flow
        where (Gsave(imin_,:,1).gt.0.5_WP) ustar(imin_,:,1) = Umag
        if (ndof.gt.1) then
           where (Gsave(imin_,:,1).gt.0.5_WP) ustar(imin_,:,2) = Umag_var
        end if
        ! Calculate Local Inflow Flux
        do k = 1,ndof
           FluxIn_local(k) = sum(ustar(imin_,jmin_:jmax_,k))
        end do
     end if

     ! Total Inflow Flux
     do k = 1,ndof
        call sum_real(FluxIn_local(k),FluxIn(k))
     end do

     ! Calculate local outflow flux
     if (rankx.eq.px-1) then
        do k = 1,ndof
           ! FluxOut_local(k) = sum(ustar(imax_+1,jmin_:jmax_,k))
           FluxOut_local(k) = sum(ustar(imax_,jmin_:jmax_,k))
        end do
     end if

     ! Total outflow flux
     do k = 1,ndof
        call sum_real(FluxOut_local(k),FluxOut(k))
     end do

     if (rankx.eq.px-1) then
        ! Scale outflow so flux in = flux out
        do k = 1,ndof
           ! ustar(imax_+1,:,k) = ustar(imax_+1,:,k) + (FluxIn(k)-FluxOut(k))/real(ny,WP)
           ustar(imax_,:,k) = ustar(imax_,:,k) + (FluxIn(k)-FluxOut(k))/real(ny,WP)
        end do
        ! Neuman Condition at outflow
        ! do i = 2,nghost
        !    ustar(imax_+i,:,:) = ustar(imax_+1,:,:)
        ! end do
        do i=1,nghost
           ustar(imax_+i,:,:) = ustar(imax_,:,:)
           vstar(imax_+i,:,:) = vstar(imax_,:,:)
        end do
     end if

  case ('OscillatingDroplet','oscillatingdroplet','OSCILLATINGDROPLET','Droplet','droplet')

     ! Set top and bottom walls to 0
     if (ranky.eq.py-1) then
        do j=1,nghost
           ustar(:,jmax_+j,:) = -ustar(:,jmax_,:)
        end do
        vstar(:,jmax_+1:jmaxo_,:) = 0.0_WP
     end if
     if (ranky.eq.0) then
        do j=1,nghost
           ustar(:,jmin_-j,:) = -ustar(:,jmin_,:)
        end do
        vstar(:,jmino_:jmin_,:) = 0.0_WP
     end if

     ! Open left and right walls
     FluxIn_local = 0.0_WP
     FluxOut_local = 0.0_WP
     if (rankx.eq.0) then
        ! Neuman condition at left
        do i=1,nghost
           ustar(imin_-i,:,:) = ustar(imin_,:,:)
        end do
        ! Calculate left flux
        do k = 1,ndof
           FluxIn_local(k) = sum(ustar(imin_,jmin_:jmax_,k))
        end do
     end if

     ! Total right side flux
     do k = 1,ndof
        call sum_real(FluxIn_local(k),FluxIn(k))
     end do

     ! Neuman condtion at right
     if (rankx.eq.px-1) then
        do i=1,nghost
           ustar(imax_+i,:,:) = ustar(imax_,:,:)
        end do
        ! Calculate right flux
        do k = 1,ndof
           FluxOut_local(k) = sum(ustar(imax_+1,jmin_:jmax_,k))
        end do
     end if

     ! Total left side flux
     do k = 1,ndof
        call sum_real(FluxOut_local(k),FluxOut(k))
     end do

     ! Scale so flux left = flux right
     if (rankx.eq.px-1) then
        do k = 1,ndof
           ! ustar(imin_,jmin_:jmax_,k) = ustar(imin_,jmin_:jmax_,k) - 0.5_WP*(FluxIn(k)-FluxOut(k))/real(ny,WP)
           ustar(imax_+1,jmin_:jmax_,k) = ustar(imax_+1,jmin_:jmax_,k) + (FluxIn(k)-FluxOut(k))/real(ny,WP)
        end do
     end if

  case ('CapFlow','capflow','Cap Flow')
     if (rankx.eq.0) then
        ustar(imino_:imin_,:,:) = 0.0_WP
        do i=1,nghost
           vstar(imin_-i,:,:) = -vstar(imin_,:,:)
        end do
     end if
     if (rankx.eq.px-1) then
        do i=1,nghost
           ustar(imax_+i,:,:) = 0.0_WP
           vstar(imax_+i,:,:) = -vstar(imax_,:,:)
        end do
     end if
     if (ranky.eq.0) then
        do j=1,nghost
           ustar(:,jmin_-j,:) = -ustar(:,jmin_,:)
        end do
        vstar(:,jmino_:jmin_,:) = 0.0_WP
     end if
     if (ranky.eq.py-1) then
        do j=1,nghost
           ustar(:,jmax_+j,:) = 2.0_WP - ustar(:,jmax_,:)
           vstar(:,jmax_+j,:) = 0.0_WP
        end do
     end if

  case ('CrossflowJet','CROSSFLOWJET','crossflowjet','Crossflowjet')

     FluxIn_local = 0.0_WP; FluxOut_local = 0.0_WP
     ! Set top and bottom wall to zero and add jet
     if (ranky.eq.py-1) then
        do j=1,nghost
           ustar(:,jmax_+j,:) = -ustar(:,jmax_,:)
        end do
        vstar(:,jmax_+1:jmaxo_,:) = 0.0_WP
     end if
     if (ranky.eq.0) then
        do j=1,nghost
           ustar(:,jmin_-j,:) = -ustar(:,jmin_,:)
        end do
        vstar(:,jmino_:jmin_,:) = 0.0_WP
        ! Incoming Jet
        do i = imino_,imaxo_
           if ((xm(i).lt.(0.5_WP*(xmax-xmin)+0.05_WP*(xmax-xmin))).and. &
                (xm(i).gt.(0.5_WP*(xmax-xmin)-0.05_WP*(xmax-xmin)))) then
              vstar(i,jmino_:jmin_,1) = 100.0_WP
           end if
        end do
        ! Calculate Local Inflow Flux
        do k = 1,ndof
           FluxIn_local(k) = FluxIn_local(k) + sum(vstar(imin_:imax_,jmin_,k))
        end do
     end if

     ! Incoming Jet Flow
     if (rankx.eq.0) then
        ! Inflow
        ustar(imino_:imin_,:,1) = 50.0_WP
        do i=1,nghost
           vstar(imin_-i,:,:) = -vstar(imin_,:,:)
        end do
        ! Calculate Local Inflow Flux
        do k = 1,ndof
           FluxIn_local(k) = FluxIn_local(k) + sum(ustar(imin_,jmin_:jmax_,k))
        end do
     end if

     ! Total Inflow Flux
     do k = 1,ndof
        call sum_real(FluxIn_local(k),FluxIn(k))
     end do

     if (rankx.eq.px-1) then
        ! Neuman Condtion at outflow
        do i=1,nghost
           ustar(imax_+i,:,:) = ustar(imax_,:,:)
        end do
        ! Calculate local outflow flux
        do k = 1,ndof
           FluxOut_local(k) = sum(ustar(imax_+1,jmin_:jmax_,k))
        end do
     end if

     ! Total outflow flux
     do k = 1,ndof
        call sum_real(FluxOut_local(k),FluxOut(k))
     end do

     ! Scale outflow so flux in = flux out
     if (rankx.eq.px-1) then
        do k = 1,ndof
           ustar(imax_+1,:,k) = ustar(imax_+1,:,k) + (FluxIn(k)-FluxOut(k))/real(ny,WP)
        end do
     end if

  case default
     call die('Unknown simulation (velocity.f90): '//trim(simu))
  end select

  ! ! Left right faces ------------
  ! if (isxper) then  ! apply periodic condition
  !    if(rankx.eq.px-1) then
  !       ustar(imax+1,:,:)=ustar(imax,:,:)
  !       vstar(imax+1,:,:)=vstar(imax,:,:)
  !    end if
  ! end if

  ! ! Top bottom faces ------------
  ! if (isyper) then  ! apply periodic condition
  !    if(ranky.eq.py-1) then
  !       ustar(:,jmax+1,:)=ustar(:,jmax,:)
  !       vstar(:,jmax+1,:)=vstar(:,jmax,:)
  !    end if

  ! else ! apply boundary condition on top and bottom walls
  !    select case(bc_y)
  !    case ('no-slip')
  !       if (ranky.eq.0)    vstar(:,jmin,1)=0.0_WP
  !       if (ranky.eq.py-1) vstar(:,jmax,1)=0.0_WP
  !       if (ranky.eq.0)    ustar(:,jmin-1,1)=-ustar(:,jmin,1)
  !       if (ranky.eq.py-1) ustar(:,jmax+1,1)=-ustar(:,jmax,1)
  !    case ('Dirichlet')
  !       ! Do nothing
  !    case default
  !       call die('Unknown boundary condition'//bc_y)
  !    end select

  ! end if

  ! Hold until all processors are done
  call MPI_BARRIER(COMM, verr)

  return
end subroutine velocity_bc




