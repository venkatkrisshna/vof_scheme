! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ------------------------------- !
!  This module handles operations !
!   involving the velocity field  !
! ------------------------------- !

module velocity
  use variables
  use grid
  implicit none

contains

  ! Calculate x-advection term !
  ! ========================== !
  function advc_x(i,j)
    implicit none
    integer, intent(in) :: i,j
    real(WP) :: advc_x

    advc_x = dxi*(((u(i+1,j)+u(i,j))*0.5_WP)**2 - ((u(i,j)+u(i-1,j))*0.5_WP)**2) + &
         dyi*(&
         (u(i,j+1)+u(i,j))*0.5_WP*(v(i,j+1)+v(i-1,j+1))*0.5_WP - &
         (u(i,j)+u(i,j-1))*0.5_WP*(v(i,j  )+v(i-1,j  ))*0.5_WP)

  end function advc_x
  
  ! Calculate y-advection term !
  ! ========================== !
  function advc_y(i,j)
    implicit none
    integer, intent(in) :: i,j
    real(WP) :: advc_y

    advc_y = dyi*(((v(i,j+1)+v(i,j))*0.5_WP)**2 - ((v(i,j)+v(i,j-1))*0.5_WP)**2) + &
         dxi*(&
         (u(i+1,j)+u(i+1,j-1))*0.5_WP*(v(i+1,j)+v(i,j))*0.5_WP - &
         (u(i  ,j)+u(i  ,j-1))*0.5_WP*(v(i,j)+v(i-1,j))*0.5_WP)
    
  end function advc_y
  
  ! Calculate x-diffusion term !
  ! ========================== !
  function diff_x(i,j)
    implicit none
    integer, intent(in) :: i,j
    real(WP) :: diff_x

    diff_x = 2.0_WP*(dxi**2)*(mu(i,j)*(u(i+1,j)-u(i,j)) - &
         (mu(i-1,j))*(u(i,j)-u(i-1,j))) + ( &
         VISC_here(i,j,i,j+1,i-1,j,i-1,j+1)* &
         ((u(i,j+1)-u(i,j  ))*dyi+(v(i,j+1)-v(i-1,j+1))*dxi) - &
         VISC_here(i,j,i,j-1,i-1,j,i-1,j-1)* &
         ((u(i,j  )-u(i,j-1))*dyi+(v(i,j  )-v(i-1,j  ))*dxi))*dyi
    
  end function diff_x

  ! Calculate y-diffusion term !
  ! ========================== !
  function diff_y(i,j)
    implicit none
    integer, intent(in) :: i,j
    real(WP) :: diff_y

    diff_y = 2.0_WP*(dyi**2)*(mu(i,j)*(v(i,j+1)-v(i,j)) - &
         (mu(i,j-1))*(v(i,j)-v(i,j-1))) + ( &
         VISC_here(i,j,i+1,j,i,j-1,i+1,j-1)* &
         ((u(i+1,j)-u(i+1,j-1))*dyi+(v(i+1,j)-v(i,j))*dxi) - &
         VISC_here(i,j,i-1,j,i,j-1,i-1,j-1)* &
         ((u(i,j)-u(i,j-1))*dyi+(v(i,j  )-v(i-1,j  ))*dxi))*dxi

  end function diff_y

  ! Interpolate viscosity to this corner point !
  ! ========================================== !
  function VISC_here(i1,j1,i2,j2,i3,j3,i4,j4)
    implicit none
    integer, intent(in) :: i1,j1,i2,j2,i3,j3,i4,j4 ! Stencil
    real(WP) :: VOF_stencil,VISC_here
    
    VOF_stencil = 0.25_WP*(VOFavg(i1,j1)+VOFavg(i2,j2)+VOFavg(i3,j3)+VOFavg(i4,j4))
    select case(trim(VISC_met))
    case ('quarter' ); VISC_here = 0.25_WP*(mu(i1,j1)+mu(i2,j2)+mu(i3,j3)+mu(i4,j4))
    case ('linear'  ); VISC_here = VOF_stencil*mu_1 + (1.0_WP - VOF_stencil)*mu_2
    case ('harmonic'); VISC_here = mu_1 - mu_2*mu_1/(mu_1*VOF_stencil + mu_2*(1.0_WP-VOF_stencil))
    end select
    
  end function VISC_here
  
end module velocity

! ============================= !
!  Inititalize velocity arrays  !
! ============================= !
subroutine velocity_init
  use velocity
  implicit none

  ! u velocity arrays
  allocate(u (imino_:imaxo_+1,jmino_:jmaxo_)); u =0.0_WP
  allocate(us(imino_:imaxo_+1,jmino_:jmaxo_)); us=0.0_WP
  allocate(u0(imino_:imaxo_+1,jmino_:jmaxo_)); u0=0.0_WP
  allocate(ui(imin_ :imax_   ,jmin_ :jmax_ )); ui=0.0_WP

  ! v velocity arrays
  allocate(v (imino_:imaxo_,jmino_:jmaxo_+1)); v =0.0_WP
  allocate(vs(imino_:imaxo_,jmino_:jmaxo_+1)); vs=0.0_WP
  allocate(v0(imino_:imaxo_,jmino_:jmaxo_+1)); v0=0.0_WP
  allocate(vi(imin_ :imax_   ,jmin_ :jmax_ )); vi=0.0_WP

  ! Source terms
  allocate(srcU(imino_:imaxo_,jmino_:jmaxo_)); srcU=0.0_WP
  allocate(srcV(imino_:imaxo_,jmino_:jmaxo_)); srcV=0.0_WP

  ! Cell divergence
  allocate(div_(imino_:imaxo_,jmino_:jmaxo_)); div_=0.0_WP

  ! Apply BCs on velocity field
  call velocity_boundary(u,v)

  return
end subroutine velocity_init

! ================ !
!  Corrector step  !
! ================ !
subroutine corrector
  use communication
  use velocity
  implicit none
  integer :: i,j

  do j = jmin_,jmax_
     do i = imin_,imax_
        u(i,j) = us(i,j) - dt*(p(i,j)-p(i-1,j))*dxi/((rho(i-1,j)+rho(i,j))*0.5_WP)
        v(i,j) = vs(i,j) - dt*(p(i,j)-p(i,j-1))*dyi/((rho(i,j-1)+rho(i,j))*0.5_WP)
     end do
  end do

  ! Update boundaries
  call communication_border(u(imin_-1:imax_+1,jmin_-1:jmax_+1))
  call communication_border(v(imin_-1:imax_+1,jmin_-1:jmax_+1))
  call velocity_boundary(u,v)

  return
end subroutine corrector

! ================ !
!  Predictor step  !
! ================ !
subroutine predictor
  use communication
  use velocity
  implicit none
  integer :: i,j
  
  call velocity_boundary(u,v)
  call CFL
  
  do j = jmin_,jmax_
     do i = imin_,imax_

        us(i,j) = u(i,j)+dt*((diff_x(i,j)+srcU(i,j))/(0.5_WP*(rho(i-1,j)+rho(i,j)))-advc_x(i,j))
        vs(i,j) = v(i,j)+dt*((diff_y(i,j)+srcV(i,j))/(0.5_WP*(rho(i,j-1)+rho(i,j)))-advc_y(i,j))
        
        ! us(i,j) = u(i,j) &
        !      + dt*(0.5_WP*(nu(i-1,j)+nu(i,j))* &
        !      (((u(i-1,j) - 2*u(i,j) + u(i+1,j))*dxi**2) &
        !      + ((u(i,j-1) - 2*u(i,j) + u(i,j+1))*dyi**2)) &
        !      - ((u(i,j)*(u(i+1,j) - u(i-1,j))*(0.5_WP*dxi)) &
        !      - (0.25_WP*(v(i-1,j) + v(i,j) + v(i-1,j+1) &
        !      + v(i,j+1))*(u(i,j+1) - u(i,j-1))*(0.5_WP*dyi))) &
        !      + srcU(i,j))
        ! vs(i,j) = v(i,j) &
        !      + dt*(0.5_WP*(nu(i,j-1)+nu(i,j))* &
        !      ((v(i-1,j) - 2*v(i,j) + v(i+1,j))*dxi**2 &
        !      + (v(i,j-1) - 2*v(i,j) + v(i,j+1))*dyi**2) &
        !      - (0.25_WP*(u(i,j-1) + u(i,j) + u(i+1,j-1) &
        !      + u(i+1,j))*(v(i+1,j) - v(i-1,j))*(0.5_WP*dxi) &
        !      + v(i,j)*(v(i,j+1) - v(i,j-1))*(0.5_WP*dyi)) &
        !      + srcV(i,j))
        
     end do
  end do
  
  ! Update boundaries
  call communication_border(us(imin_-1:imax_+1,jmin_-1:jmax_+1))
  call communication_border(vs(imin_-1:imax_+1,jmin_-1:jmax_+1))
  call velocity_boundary(us,vs)

  return
end subroutine predictor

! =============== !
!  CFL condition  !
! =============== !
subroutine CFL
  use parallel
  use communication
  use velocity
  implicit none
  integer :: ierr
  real(WP) :: dt_cfl
  real(WP) :: maxvel
  real(WP) :: v_cfl
  
  dt_cfl = huge(1.0_WP)
  call MPI_ALLREDUCE(maxval(u(imin_:imax_+1,jmin_:jmax_)), Umax,&
       1, mpi_double_precision, mpi_max, comm, ierr)
  call MPI_ALLREDUCE(maxval(v(imin_:imax_,jmin_:jmax_+1)), Vmax,&
       1, mpi_double_precision, mpi_max, comm, ierr)
  maxvel = max(Umax,Vmax)
  ! Convective CFL
  cCFLmax = maxvel*dt/dx
  ! Viscous CFL
  vCFLmax = dt*2*maxval(mu)/dx**2
  if (vCFLmax.gt.1.0_WP) print*,'High viscous CFL!'
  if (maxvel.gt.0) dt_cfl = CFLreq*dx/maxvel
  if (dt_cfl.lt.dt) dt = dt_cfl

  return
end subroutine CFL

! ================================ !
!  Compute divergence of velocity  !
! ================================ !
subroutine divergence
  use parallel
  use velocity
  implicit none
  
  integer :: ierr
  integer :: i,j

  do j = jmin_,jmax_
     do i = imin_,imax_
        div_(i,j) = (((u(i+1,j) - u(i,j))*dxi) + ((v(i,j+1) - v(i,j))*dyi))
     end do
  end do
  call MPI_ALLREDUCE(maxval(div_), div, 1, mpi_double_precision, mpi_max, comm, ierr)

  return
end subroutine divergence

! ================================== !
!  Check for steady state condition  !
! ================================== !
subroutine steady_state
  use communication
  use parallel
  use velocity
  implicit none
  integer :: ierr
  integer :: i,j
  real(WP) :: tolsum,maxtol

  ! Initialize
  tolsum=0.0_WP
  maxtol=0.0_WP

  ! Sum of tolerances in u
  do j = jmin_,jmax_
     do i = imin_,imax_+1
        tolsum = tolsum + abs(u(i,j)-u0(i,j))
     end do
  end do
  ! Sum of tolerances in v
  do j = jmin_,jmax_+1
     do i = imin_,imax_
        tolsum = tolsum + abs(v(i,j)-v0(i,j))
     end do
  end do
  ! Find total tolerance in velocity fields
  call MPI_ALLREDUCE(maxval(u), Umax  , 1, mpi_double_precision, mpi_max, comm, ierr)
  call MPI_ALLREDUCE(maxval(v), Vmax  , 1, mpi_double_precision, mpi_max, comm, ierr)
  call MPI_ALLREDUCE(tolsum   , maxtol, 1, mpi_double_precision, mpi_sum, comm, ierr)
  maxtol = maxtol/max(Umax,Vmax)
  conv = maxtol

  if (maxtol.lt.tolv .and. irank.eq.1) print*,'Velocity field has converged!'

  ! Re-initialize
  u0 = u
  v0 = v

  return
end subroutine steady_state

! =========================================== !
!  Compute cell centered u, v velocity field  !
! =========================================== !
subroutine velocity_interpolation
  use velocity
  implicit none
  integer :: i,j

  ui(imin_,jmin_:jmax_) = u(imin_  ,jmin_:jmax_)
  ui(imax_,jmin_:jmax_) = u(imax_+1,jmin_:jmax_)
  do i = imin_+1,imax_-1
     ui(i,jmin_:jmax_) = (u(i+1,jmin_:jmax_) + u(i,jmin_:jmax_))*0.5_WP
  end do

  vi(imin_:imax_,jmin_) = v(imin_:imax_,jmin_  )
  vi(imin_:imax_,jmax_) = v(imin_:imax_,jmax_+1)
  do j = jmin_+1,jmax_-1
     vi(imin_:imax_,j) = (v(imin_:imax_,j+1) + v(imin_:imax_,j))*0.5_WP
  end do

  return
end subroutine velocity_interpolation
