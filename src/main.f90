! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!                                 !
!       This program is a 2D      !
!    incompressible flow solver   !
!     Please read the attached    !
!    documentation before using   !
!           the solver            !
!                                 !
! ------------------------------- !
!        MAIN SOLVER PROGRAM      !
! ------------------------------- !

program main
  implicit none
  
  ! Initialize the program
  call initialize

  ! Run core program
  call core
  
  ! Terminate simulation and end program
  call terminate
  
end program main

! =================== !
!  Initialize Solver  !
! =================== !
subroutine initialize
  use io
  use variables
  use parallel
  use grid
  use pressure
  use silo
  implicit none

  ! Read input file
  call io_init

  ! Initialize core variables
  call io_read_input

  ! Initiate parallel environment
  call parallel_init

  ! Initiate and partition grid
  call grid_init

  ! Initiate pressure solver
  call vof_init

  ! Initiate boundary conditions
  call boundary_init
  
  ! Initiate velocity field
  call velocity_init

  ! Initiate fluid property handling
  call property_init

  ! Initiate pressure solver
  call pressure_init

  ! Initiate SILO output
  call silo_init

  ! Initiate command window output
  if (irank.eq.1) call io_output_header

  return
end subroutine initialize

! ============== !
!  Core Program  !
! ============== !
subroutine core
  use variables
  use io
  use parallel
  use grid
  use pressure
  use velocity
  use silo
  implicit none

  do iter = 1,nsteps

     ! Predictor step
     call predictor
     
     ! Pressure solver
     call pressure_solve
     
     ! Corrector step
     call corrector

     ! Advect fluid interface
     call vof_step

     ! Update properties
     call property_update

     ! Compute divergence
     call divergence

     ! Track progress and output data
     call track_progress

     ! Write data to SILO file
     if (mod(step,out_freq).eq.0) call silo_step
     
     ! ! Check for steady state
     ! call steady_state
  
     ! ! Quit iterating if velocity has converged
     ! if (conv.lt.tolv) exit

     ! Quit iterating if time exceeds
     if (t.gt.term) exit

  end do

  return
end subroutine core

! ================== !
!  Terminate solver  !
! ================== !
subroutine terminate
  use variables
  use parallel
  implicit none
  
  integer :: ierr
  
  call MPI_BARRIER(comm,ierr)

  if (irank.eq.1) then
  print*,' --------------------------------------------------------------------------',&
       '---------------------'
     print*,'                                           '&
          ,'Program Complete!'
  print*,' --------------------------------------------------------------------------',&
       '---------------------'
  end if
  call MPI_FINALIZE(ierr)

  return
end subroutine terminate

! ============= !
!  Kill solver  !
! ============= !
subroutine die(text)
  implicit none
  integer :: ierr,comm

  character(len=*), intent(in) :: text
  print *,'!!! PROGRAM HAS BEEN KILLED !!! ',trim(text)
  call MPI_ABORT(comm,ierr)

  return
end subroutine die

! ================== !
!  Print data field  !
! ================== !
subroutine print_field(data_field,procid)
  use variables
  use parallel
  use vof
  implicit none
  character(len=*), intent(in) :: data_field
  integer, intent(in) :: procid
  integer :: j
  
  if (irank.eq.procid) then
     print*,''
     print*,'                           Printing Data Field '&
          ,trim(data_field),' from Processor',irank
     print*,''

     select case(trim(data_field))
     case('U*')
        do j = jmax_,jmin_,-1
           print*,us(imin_:imax_+1,j)
        end do
     case('U')
        do j = jmax_,jmin_,-1
           print*,u(imin_:imax_+1,j)
        end do
     case('V*')
        do j = jmax_+1,jmin_,-1
           print*,vs(imin_:imax_,j)
        end do
     case('V')
        do j = jmax_+1,jmin_,-1
           print*,v(imin_:imax_,j)
        end do
     case('P')
        do j = jmax_,jmin_,-1
           print*,p(imin_:imax_,j)
        end do
     case('VOFavg')
        do j = jmax_,jmin_,-1
           print*,VOFavg(imin_:imax_,j)
        end do
     end select
     
     print*,''
  end if

  return
end subroutine print_field
