! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ------------------------------- !
!       This module handles       !
!    modeling surface tension     !
! ------------------------------- !

module tension
  use variables
  use grid
  implicit none
  real(WP),dimension(:,:),allocatable :: kappa
  real(WP),dimension(:,:,:),allocatable :: normal_u,normal_v

contains

  ! Calculate x-component of non-unit normal at u face !
  ! ================================================== !
  function mxu(i,j)
    implicit none
    integer, intent(in) :: i,j
    real(WP) :: mxu
    mxu = -(VOFavg(i,j)-VOFavg(i-1,j))*dxi
  end function mxu

  ! Calculate x-component of non-unit normal at v face !
  ! ================================================== !
  function mxv(i,j)
    implicit none
    integer, intent(in) :: i,j
    real(WP) :: mxv
    mxv = 0.25_WP*(mxu(i+1,j)+mxu(i+1,j-1)+mxu(i,j)+mxu(i,j-1))
  end function mxv

  ! Calculate y-component of non-unit normal at u face !
  ! ================================================== !
  function myu(i,j)
    implicit none
    integer, intent(in) :: i,j
    real(WP) :: myu
    myu = 0.25_WP*(myv(i,j)+myv(i,j+1)+myv(i-1,j)+myv(i-1,j+1))
  end function myu

  ! Calculate y-component of non-unit normal at v face !
  ! ================================================== !
  function myv(i,j)
    implicit none
    integer, intent(in) :: i,j
    real(WP) :: myv
    myv = -(VOFavg(i,j)-VOFavg(i,j-1))*dyi
  end function myv
  
end module tension

! ============================ !
!  Inititate curvature arrays  !
! ============================ !
subroutine tension_init
  use tension
  implicit none

  allocate(kappa(imino_:imaxo_,jmino_:jmaxo_)); kappa=0.0_WP
  allocate(normal_u(imino_:imaxo_+1,jmino_:jmaxo_,1:2)); normal_u=0.0_WP
  allocate(normal_v(imino_:imaxo_,jmino_:jmaxo_+1,1:2)); normal_v=0.0_WP

  ! Set initial properties
  call tension_update

  return
end subroutine tension_init

! ============================ !
!  Inititate curvature arrays  !
! ============================ !
subroutine tension_update
  use tension
  implicit none

  ! Set initial properties
  call tension_normal
  call tension_curv

  ! Communicate borders
  call communication_border(kappa(imin_-1:imax_+1,jmin_-1:jmax_+1))
  call boundary_domain_neumann(kappa)

  ! Add to source term
  call tension_source

  return
end subroutine tension_update

! ==================================== !
!  Compute unit normals at cell faces  !
! ==================================== !
subroutine tension_normal
  use vof
  use property
  use velocity
  use tension
  implicit none
  integer :: i,j

  normal_u = 0.0_WP; normal_v = 0.0_WP
  
  do j = jmin_,jmax_
     do i = imin_,imax_
        normal_u(i,j,1) = mxu(i,j)/(sqrt(mxu(i,j)**2+myu(i,j)**2)+epsilon(1.0_WP)) ! mxu
        normal_u(i,j,2) = myu(i,j)/(sqrt(mxu(i,j)**2+myu(i,j)**2)+epsilon(1.0_WP)) ! mxv
        normal_v(i,j,1) = mxv(i,j)/(sqrt(mxv(i,j)**2+myv(i,j)**2)+epsilon(1.0_WP)) ! myu
        normal_v(i,j,2) = myv(i,j)/(sqrt(mxv(i,j)**2+myv(i,j)**2)+epsilon(1.0_WP)) ! myv
     end do
  end do

  return
end subroutine tension_normal

! ========================= !
!  Determination curvature  !
! ========================= !
subroutine tension_curv
  use vof
  use tension
  implicit none
  integer :: i,j
  real(WP) :: hf(3)    ! Height functions (-1, 0,+1)
  real(WP) :: hd,hdd   ! First and second derivative of the height function
  real(WP) :: delta

  kappa = 0.0_WP

  select case(trim(CURV_met))
     
  case('HFM')
     ! Using height function method
     ! --------------------------------
     do j = jmin_,jmax_
        do i = imin_,imax_
           hf = 0.0_WP
           if     (abs(VOFslop(i,j)).gt.1.0_WP) then ! is more vertical
              hf(1) = sum(VOFavg(i-3:i+3,j-1))
              hf(2) = sum(VOFavg(i-3:i+3,j  ))
              hf(3) = sum(VOFavg(i-3:i+3,j+1))
              delta = dy
           elseif (abs(VOFslop(i,j)).le.1.0_WP) then ! or more horizontal
              hf(1) = sum(VOFavg(i-1,j-3:j+3))
              hf(2) = sum(VOFavg(i  ,j-3:j+3))
              hf(3) = sum(VOFavg(i+1,j-3:j+3))
              delta = dx
           end if
           ! Compute derivatives of height function
           hd  = (hf(3)-hf(1))/(2.0_WP*delta)
           hdd = (hf(1) - 2.0_WP*hf(2) + hf(3))/delta**2
           ! Compute curvature kappa
           kappa(i,j) = hdd/(1.0_WP+hd**2)**1.5
        end do
     end do

  case('divnorm')
     ! Using divergence of unit normal
     ! -------------------------------
     do j = jmin_,jmax_
        do i = imin_,imax_
           kappa(i,j) = -(&
                dxi*(normal_u(i+1,j,1)-normal_u(i,j,1)) + &
                dyi*(normal_v(i,j+1,2)-normal_v(i,j,2)))
        end do
     end do

  end select

  return
end subroutine tension_curv

! ========================================== !
!  Add surface tension force to source term  !
! ========================================== !
subroutine tension_source
  use vof
  use property
  use velocity
  use tension
  implicit none
  integer :: i,j

  ! Reset source terms to zero
  srcU = 0.0_WP
  srcV = 0.0_WP

  do j = jmin_,jmax_
     do i = imin_,imax_
        srcU(i,j) = -sigma*0.5_WP*dxi*(VOFavg(i,j)-VOFavg(i-1,j))*(kappa(i,j)+kappa(i-1,j))
        srcV(i,j) = -sigma*0.5_WP*dyi*(VOFavg(i,j)-VOFavg(i,j-1))*(kappa(i,j)+kappa(i,j-1))
     end do
  end do

  return
end subroutine tension_source
