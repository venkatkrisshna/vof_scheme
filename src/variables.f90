! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ---------------------------------
!   This module declares global   !
!   variables being used in the   !
!              solver             !
! ---------------------------------

module variables
  implicit none

  ! Precision
  integer, parameter :: SP = kind(1.0)
  integer, parameter :: WP = kind(1.0d0)

  ! MPI objects
  integer :: npx,npy
  
  ! Domain objects
  real(WP) :: Lx,Ly
  integer :: nx,ny,nx_,ny_,nxo_,nyo_
  integer :: imin  , imax  , jmin  , jmax
  integer :: imino , imaxo , jmino , jmaxo
  integer :: imin_ , imax_ , jmin_ , jmax_
  integer :: imino_, imaxo_, jmino_, jmaxo_
  integer :: nover
  
  ! Simulation parameter objects
  integer :: step=0, nsteps, iter
  real(WP) :: conv=1.0_WP
  real(WP) :: CFLreq
  real(WP) :: dt
  real(WP) :: term
  real(WP) :: t=0.0_WP
  real(WP),dimension(:,:),allocatable :: div_
  logical :: xper=.false.,yper=.false.
  
  ! Density
  real(WP) :: rho_1,rho_2
  real(WP),dimension(:,:),allocatable :: rho
  character(len=30) :: DENS_met ! Density averaging method

  ! Viscosities
  real(WP) :: mu_1,mu_2 ! Dynamic
  real(WP),dimension(:,:),allocatable :: mu
  character(len=30) :: VISC_met ! Viscosity averaging method

  ! Surface tension
  real(WP) :: sigma
  logical :: use_HF=.false.
  character(len=30) :: CURV_met ! Curvature estimation method
  
  ! Velocity objects
  real(WP),dimension(:,:),allocatable :: u , v
  real(WP),dimension(:,:),allocatable :: us, vs
  real(WP),dimension(:,:),allocatable :: u0, v0
  real(WP),dimension(:,:),allocatable :: ui, vi
  real(WP) :: tolv
  character(len=30) :: simulation
  real(WP) :: utop,vtop,ubot,vbot,uleft,uright,vleft,vright
  
  ! Pressure objects
  real(WP),dimension(:,:),allocatable :: p
  real(WP) :: tolp
  character(len=20) :: pressure_solver
  character(len=20) :: pressure_precond
  integer :: max_piter

  ! Source terms
  real(WP),dimension(:,:),allocatable :: srcU,srcV
  
  ! Output objects
  integer :: out_freq
  real(WP) :: Umax,Vmax
  real(WP) :: cCFLmax,vCFLmax
  real(WP) :: div
  integer :: piter
  
  ! VOF objects
  logical :: use_vof=.false.
  real(WP),dimension(:,:),allocatable :: VOFavg
  character(len=50) :: vofcase
  real(WP) :: Cx,Cy,Rx,Ry
  
end module variables
