! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ------------------------------- !
!       This module initiates     !
!      parallel infrastructure    !
!             using MPI           !
! ------------------------------- !

module parallel
  use mpi
  implicit none

  ! MPI objects
  integer :: irank, numproc, comm
  integer :: iproc, jproc
  integer :: up, down, left, right

end module parallel

! =============================== !
!  INITIATE PARALLEL ENVIRONMENT  !
! =============================== !
subroutine parallel_init
  use parallel
  use variables
  implicit none
  integer :: ierr
  integer, dimension(2) :: dims
  logical, dimension(2) :: isper
  logical :: reorder
  integer :: ndims
  integer, dimension(2) :: coords

  call MPI_INIT(ierr)
  call MPI_COMM_RANK(mpi_comm_world,irank,ierr)
  call MPI_COMM_SIZE(mpi_comm_world,numproc,ierr)
  if (numproc.ne.npx*npy .and. irank.eq.1) then
     call die('Incorrect Number of Processors Specified!')
  end if
  comm = mpi_comm_world
  ! Set MPI topology
  ndims = 2
  dims(1) = npx
  dims(2) = npy
  isper   = .false.
  reorder = .true.
  ! Create Cartesian grid for parallel computing
  call MPI_CART_CREATE(mpi_comm_world,ndims,dims,isper,reorder,comm,ierr)
  call MPI_CART_COORDS(comm,irank,ndims,coords,ierr)
  call MPI_CART_SHIFT(comm,1,1,up,down,ierr)
  call MPI_CART_SHIFT(comm,0,1,left,right,ierr)
  irank = irank + 1
  iproc = coords(1) + 1
  jproc = coords(2) + 1

  return
end subroutine parallel_init
