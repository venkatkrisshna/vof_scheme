! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ------------------------------- !
!        This module handles      !
!   communication between procs   !
! ------------------------------- !

module communication
  implicit none

end module communication

! ================================ !
!  Communicate processor boundary  !
! ================================ !
subroutine communication_border(comm_data)
  use variables
  use communication
  implicit none
  real(WP), dimension(imin_-1:imax_+1,jmin_-1:jmax_+1) :: comm_data
  
  if (npx.gt.1) call communication_border_x(comm_data)
  if (npy.gt.1) call communication_border_y(comm_data)

  return
end subroutine communication_border

! ========================== !
!  Communicate data along X  !
! ========================== !
subroutine communication_border_x(comm_data)
  use communication
  use variables
  use grid
  use parallel
  implicit none
  real(WP), dimension(imin_-1:imax_+1,jmin_-1:jmax_+1) :: comm_data
  real(WP), dimension(:),allocatable :: buf1,buf2
  integer :: ierr,status(MPI_STATUS_SIZE)
  integer :: comm_size
  integer :: MPI_DP=MPI_DOUBLE_PRECISION
  
  comm_size = size(comm_data(imax_,:)) ! Size of message

  allocate(buf1(comm_size)); buf1 = 0.0_WP
  allocate(buf2(comm_size)); buf2 = 0.0_WP

  ! Send to left, recv from right
  buf1 = comm_data(imin_,:)
  call MPI_SENDRECV(buf1, comm_size, MPI_DP, left , 10, &
       buf2, comm_size, MPI_DP, right, 10, comm, status, ierr)
  if (right.NE.MPI_PROC_NULL) comm_data(imax_+1,:) = buf2

  ! Send to right, recv from left
  buf1 = comm_data(imax_,:)
  call MPI_SENDRECV(buf1, comm_size, MPI_DP, right, 20, &
       buf2, comm_size, MPI_DP, left , 20, comm, status, ierr)
  if (left .NE.MPI_PROC_NULL) comm_data(imin_-1,:) = buf2

  deallocate(buf1)
  deallocate(buf2)
  
  return
end subroutine communication_border_x

! ========================== !
!  Communicate data along Y  !
! ========================== !
subroutine communication_border_y(comm_data)
  use communication
  use variables
  use grid
  use parallel
  implicit none
  real(WP), dimension(imin_-1:imax_+1,jmin_-1:jmax_+1) :: comm_data
  real(WP), dimension(:),allocatable :: buf1,buf2
  integer :: ierr,status(MPI_STATUS_SIZE)
  integer :: comm_size
  integer :: MPI_DP=MPI_DOUBLE_PRECISION
  
  comm_size = size(comm_data(:,jmax_)) ! Size of message

  allocate(buf1(comm_size)); buf1 = 0.0_WP
  allocate(buf2(comm_size)); buf2 = 0.0_WP

  ! Send to up, recv from down
  buf1 = comm_data(:,jmin_)
  call MPI_SENDRECV(buf1, comm_size, MPI_DP, up  , 30, &
       buf2, comm_size, MPI_DP, down, 30, comm, status, ierr)
  if (down.NE.MPI_PROC_NULL) comm_data(:,jmax_+1) = buf2

  ! Send to down, recv from up
  buf1 = comm_data(:,jmax_)
  call MPI_SENDRECV(buf1, comm_size, MPI_DP, down, 40, &
       buf2, comm_size, MPI_DP, up  , 40, comm, status, ierr)
  if (up  .NE.MPI_PROC_NULL) comm_data(:,jmin_-1) = buf2

  deallocate(buf1)
  deallocate(buf2)

  return
end subroutine communication_border_y
