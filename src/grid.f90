! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ------------------------------- !
!     This module creates the     !
!       computational domain      !
! ------------------------------- !

!=======================================================!
!                                                       !
!                 A CELL IN THIS DOMAIN                 !
!                                                       !
!         _____________ v(i,j+1) ____________      ...  !
!        |                                   |      |   !
!        |                                   |      |   !
!        |                                   |      |   !
!        |                                   |      |   !
!        |                                   |      |   !
!        |                                   |      |   !
!                        P(i,j)                     |   !
!     u(i,j)            VOF(i,j)          u(i+1,j)  |   !
!                                                   |   !
!        |              ui (i,j)             |      |   !
!        |              vi (i,j)             |      |   !
!        |                                   |          !
!        |                                   |     dy   !
!        |                                   |          !
!        |                                   |      |   !
!        |                                   |      |   !
!        |______________ v(i,j) _____________|     ...  !
!                                                       !
!        .                                   .          !
!        .---------------- dx ---------------.          !
!        .                                   .          !
!                                                       !
!=======================================================!

module grid
  use variables
  implicit none

  ! Domain objects
  real(WP) :: dx,dy,dxi,dyi
  real(WP), dimension(:), allocatable :: x , y
  real(WP), dimension(:), allocatable :: xm, ym
  integer :: imini_,imaxi_
  integer :: jmini_,jmaxi_
  
end module grid

! ============================= !
!  Initiate and Partition Grid  !
! ============================= !
subroutine grid_init
  use grid
  use parallel
  use variables
  implicit none
  integer :: i,j
  integer :: q,r

  ! Set global indices
  imino = 1
  imin  = imino + nover
  imax  = imin  + nx - 1
  imaxo = imax  + nover
  jmino = 1
  jmin  = jmino + nover
  jmax  = jmin  + ny - 1
  jmaxo = jmax  + nover

  ! Partitioning in X
  q = nx/npx
  r = mod(nx,npx)
  if (iproc<=r) then
     nx_   = q+1
     imin_ = imin + (iproc-1)*(q+1)
  else
     nx_   = q
     imin_ = imin + r*(q+1) + (iproc-r-1)*q
  end if
  nxo_   = nx_ + 2*nover
  imax_  = imin_ + nx_ - 1
  imino_ = imin_ - nover
  imaxo_ = imax_ + nover

  ! Partitioning in Y
  q = ny/npy
  r = mod(ny,npy)
  if (jproc<=r) then
     ny_   = q+1
     jmin_ = jmin + (jproc-1)*(q+1)
  else
     ny_   = q
     jmin_ = jmin + r*(q+1) + (jproc-r-1)*q
  end if
  nyo_   = ny_ + 2*nover
  jmax_  = jmin_ + ny_ - 1
  jmino_ = jmin_ - nover
  jmaxo_ = jmax_ + nover

  dx  = Lx/real(nx,WP)
  dy  = Ly/real(ny,WP)
  dxi = 1.0_WP/dx
  dyi = 1.0_WP/dy

  ! Interior index extents
  imini_ = imin_
  imaxi_ = imax_+1
  jmini_ = jmin_
  jmaxi_ = jmax_+1
  if (iproc.eq.1  ) imini_ = imin_+1
  if (iproc.eq.npx) imaxi_ = imax_
  if (jproc.eq.1  ) jmini_ = jmin_+1
  if (jproc.eq.npy) jmaxi_ = jmax_

  ! Allocate Arrays
  allocate(x(imino:imaxo+1))
  allocate(y(jmino:jmaxo+1))
  allocate(xm (imino:imaxo))
  allocate(ym (jmino:jmaxo))

  ! Create the grid
  do i=imino,imaxo+1
     x(i) = real(i-imin,WP)/real(nx,WP)*Lx
  end do
  do j=jmino,jmaxo+1
     y(j) = real(j-jmin,WP)/real(ny,WP)*Ly
  end do

  ! Compute location of cell centers
  do i=imino,imaxo
     xm(i) = 0.5_WP*(x(i)+x(i+1))
  end do
  do j=jmino,jmaxo
     ym(j) = 0.5_WP*(y(j)+y(j+1))
  end do

  return
end subroutine grid_init
