! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ------------------------------- !
!    This module handles fluid    !
!    interface advection using    !
!      the VOF (SLIC) method      !
! ------------------------------- !
! VOF Initial Conditions -        !
! - halftank                      !
! - fulltank                      !
! - brokendam                     !
! - droplet (specify x,y,r)       !
! - square (specify x,y,r)        !
! ------------------------------- !

module vof
  use variables
  implicit none

  real(WP),dimension(:,:),allocatable :: VOFslop,VOFalph
  real(WP),dimension(:,:,:),allocatable :: VOFnorm,VOFflux
  real(WP), parameter :: VOF_lo=0.0000000001_WP
  real(WP), parameter :: VOF_hi=0.9999999999_WP
  
end module vof

! ===================== !
!  Initiate VOF method  !
! ===================== !
subroutine vof_init
  use variables
  use vof
  implicit none
  
  ! Allocate VOF arrays
  allocate(VOFavg (imino_:imaxo_,jmino_:jmaxo_));     VOFavg =0.0_WP
  allocate(VOFslop(imino_:imaxo_,jmino_:jmaxo_));     VOFslop=0.0_WP
  allocate(VOFalph(imino_:imaxo_,jmino_:jmaxo_));     VOFalph=0.0_WP
  allocate(VOFflux(imino_:imaxo_,jmino_:jmaxo_,1:2)); VOFflux=0.0_WP
  allocate(VOFnorm(imino_:imaxo_,jmino_:jmaxo_,1:2)); VOFnorm=0.0_WP

  ! Initialize VOF condition
  call vof_init_fill

  ! Bookkeeping adjustments
  call vof_adjust

  ! Communicate VOF across processor boundaries
  call communication_border(VOFavg(imin_-1:imax_+1,jmin_-1:jmax_+1))

  ! Compute interface slope
  call vof_slope

  ! Compute SLIC normal
  call vof_normal
  
  ! Reconstruct interface
  call vof_reconstruct

  return
end subroutine vof_init

! ================== !
!  Perform VOF step  !
! ================== !
subroutine vof_step
  use parallel
  use vof
  implicit none

  ! Communicate VOF across processor boundaries
  call communication_border(VOFavg(imin_-1:imax_+1,jmin_-1:jmax_+1))

  ! Compute fluxes
  call vof_flux

  ! Communicate flux across processor boundaries
  call communication_border(VOFflux(imin_-1:imax_+1,jmin_-1:jmax_+1,1))
  call communication_border(VOFflux(imin_-1:imax_+1,jmin_-1:jmax_+1,2))

  ! Advect fluid
  call vof_advect

  ! Bookkeeping adjustments
  call vof_adjust

  ! Compute interface slope
  call vof_slope

  ! Compute SLIC normal
  call vof_normal
  
  ! Reconstruct interface
  call vof_reconstruct

  return
end subroutine vof_step

! ================================= !
!  Initiate fluid region in domain  !
! ================================= !
subroutine vof_init_fill
  use variables
  use grid
  use parallel
  use vof
  implicit none
  integer :: i,j
  integer :: im,jm
  integer :: icount,jcount
  real(WP) :: hdiag
  real(WP) :: rj,ri
  real(WP) :: a,b,r
  real(WP) :: yint,x_1,x_2
  real(WP),dimension(:,:),allocatable :: VOFx,VOFy

  select case(trim(vofcase))

  case('halftank')
     if (y(jmax).le.ly/2) then
        VOFavg(imin_:imax_,jmin_:jmax_) = 1.0_WP
     else if (y(jmin).le.ly/2) then
        jcount = jmin
        do j = jmin_,jmax_
           if (y(jcount).lt.ly/2) VOFavg(imin_:imax_,j) = 1.0_WP
           jcount = jcount+1
        end do
     end if
     xper = .true.

  case('fulltank')
     VOFavg(imin_:imax_,jmin_:jmax_) = 1.0_WP

  case('brokendam')
     if (iproc.eq.1.and.jproc.eq.1) then
        if (y(jmax).le.ly/2) then
           VOFavg(int(imax_*0.25):int(imax_*0.5),jmin_:jmax_) = 1.0_WP
        else if (y(jmin).le.Ly/2) then
           jcount = jmin
           do j = jmin_,jmax_
              if (y(jcount).le.Ly/2) VOFavg(int(imax_*0.25):int(imax_*0.5),j) = 1.0_WP
              jcount = jcount+1
           end do
        end if
     end if

  case('square')
     ! Allocate
     allocate(VOFx(imino_:imaxo_,jmino_:jmaxo_)); VOFx=0.0_WP
     allocate(VOFy(imino_:imaxo_,jmino_:jmaxo_)); VOFy=0.0_WP
     do i = imin_,imax_
        do j = jmin_,jmax_
           ! X-Pass
           VOFx(i,j) = min(max((min(Cx+Rx,x(i+1))-max(x(i),Cx-Rx))*(y(j+1)-y(j))*dxi*dyi,0.0_WP),1.0_WP)
           ! Y-Pass
           VOFy(i,j) = min(max((min(Cy+Rx,y(j+1))-max(y(j),Cy-Rx))*(x(i+1)-x(i))*dxi*dyi,0.0_WP),1.0_WP)
           ! Compute cell VOF
           VOFavg(i,j) = VOFx(i,j)*VOFy(i,j)
        end do
     end do
     ! Deallocate
     deallocate(VOFx,VOFy)

  case('droplet')
     ! Allocate
     allocate(VOFx(imino_:imaxo_,jmino_:jmaxo_)); VOFx=0.0_WP
     allocate(VOFy(imino_:imaxo_,jmino_:jmaxo_)); VOFy=0.0_WP
     ! X Pass
     do j = jmin_,jmax_
        ! Loop over rows that contain droplet
        if (abs(y(j)-Cy).le.Ry) then
           ! Radius of an equivalent rectangular region for this row
           rj = sqrt((Rx**2)*(1.0_WP - ((ym(j)-Cy)**2)/Ry**2))
           do i = imin_,imax_
              if (x(i).ge.Cx) exit
              VOFx(i,j) = max(min((x(i+1)-(Cx-rj))*dxi,1.0_WP),0.0_WP)
           end do
           do im = i,imax_
              VOFx(im,j) = max(min((Cx+rj-x(im))*dxi,1.0_WP),0.0_WP)
           end do
        end if
     end do
     ! Y Pass
     do i = imin_,imax_
        ! Loop over rows that contain droplet
        if (abs(x(i)-Cx).le.Rx) then
           ! Radius of an equivalent rectangular region for this col
           ri = sqrt((Ry**2)*(1.0_WP - ((xm(i)-Cx)**2)/Rx**2))
           do j = jmin_,jmax_
              if (y(j).ge.Cy) exit
              VOFy(i,j) = max(min((y(j+1)-(Cy-ri))*dyi,1.0_WP),0.0_WP)
           end do
           do jm = j,jmax_
              VOFy(i,jm) = max(min((Cy+ri-y(jm))*dyi,1.0_WP),0.0_WP)
           end do
        end if
     end do
     ! Compute cell VOF
     do j = jmin_,jmax_
        do i = imin_,imax_
           VOFavg(i,j) = VOFx(i,j)*VOFy(i,j)
        end do
     end do
     ! Deallocate
     deallocate(VOFx,VOFy)
     
  case('droplet2')
     do j=jmin_,jmax_
        do i=imin_,imax_

           ! Find Integration bounds
           ! -----------------------
           yint = Cy + sqrt(Ry**2.0_WP*(1.0_WP-(x(i)-Cx)**2.0_WP/Rx**2.0_WP))
           if (yint.gt.0.0_WP) then
              if (yint.lt.y(j)) then
                 ! Cell in gas phase
                 VOFavg(i,j) = VOFavg(i,j)+0.0_WP
                 cycle
              else if (yint.lt.y(j+1)) then
                 ! Intersection on left face
                 x_1 = x(i)
              else
                 ! Intersection on top face
                 x_1 = Cx + sqrt(Rx**2.0_WP*(1.0_WP-(y(j+1)-Cy)**2.0_WP/Ry**2.0_WP))
              end if
           else
              ! Cell in gas phase
              VOFavg(i,j) = VOFavg(i,j)+0.0_WP
              cycle
           end if

           yint = Cy + sqrt(Ry**2.0_WP*(1.0_WP-(x(i+1)-Cx)**2.0_WP/Rx**2.0_WP))
           if (yint.gt.0.0_WP) then
              if (yint.gt.y(j+1)) then 
                 ! Cell in liquid phase
                 VOFavg(i,j) = VOFavg(i,j)+1.0_WP
                 cycle
              else if (yint.gt.y(j)) then
                 ! Intersection on right face
                 x_2 = x(i+1)
              else
                 ! Intersection on bottom face
                 x_2 = Cx + sqrt(Rx**2.0_WP*(1.0_WP-(y(j)-Cy)**2.0_WP/Ry**2.0_WP))
              end if
           else
              ! Intersection on bottom face
              x_2 = Cx + sqrt(Rx**2.0_WP*(1.0_WP-(y(j)-Cy)**2.0_WP/Ry**2.0_WP))
           end if
           
           ! Integrate
           ! ---------
           VOFavg(i,j)= VOFavg(i,j) &
                ! Part to left of x_1
                + ( (x_1-x(i))*(y(j+1)-y(j)) &
                ! Integrate from x1 to x2
                + (((Cx+Rx)*(Cy+Ry)*asin(x_2/(Cx+Rx)))/2.0_WP + &
                ((Cy+Ry)*x_2*sqrt((Cx+Rx)**2.0_WP - x_2**2.0_WP))/(2.0_WP*(Cx+Rx))) &
                - (((Cx+Rx)*(Cy+Ry)*asin(x_1/(Cx+Rx)))/2.0_WP + &
                ((Cy+Ry)*x_1*sqrt((Cx+Rx)**2.0_WP - x_1**2.0_WP))/(2.0_WP*(Cx+Rx))) &
                ! Remove region below cell and between x1 and x2
                - (y(j)*(x_2-x_1))) &
                ! Normalize by area of this cell
                *dxi*dyi

        end do
     end do
     
  case('onecell')
     VOFavg(imin,jmin) = 1.0_WP

  end select

end subroutine vof_init_fill

! =========================== !
!  SLIC Normal Determination  !
! =========================== !
subroutine vof_normal
  use grid
  use parallel
  use vof
  implicit none
  real(WP) :: mxtl,mytl,mxtr,mytr,mxbl,mybl,mxbr,mybr,mx,my,m
  integer :: i,j

  VOFnorm = 0.0_WP
  
  ! Using Young's method to compute the normal
  do j = jmin_,jmax_
     do i = imin_,imax_
        ! Identify if interface exists here
        if (VOFavg(i,j).gt.VOF_lo .and. VOFavg(i,j).lt.VOF_hi) then
        ! if (any(VOFavg(i-1:i+1,j-1:j+1).lt.VOF_lo) .and. VOFavg(i,j).gt.VOF_lo) then
           mxtr = -(VOFavg(i+1,j+1)+VOFavg(i+1,j  )-VOFavg(i  ,j+1)-VOFavg(i  ,j  ))*0.5_WP*dxi
           mytr = -(VOFavg(i+1,j+1)-VOFavg(i+1,j  )+VOFavg(i  ,j+1)-VOFavg(i  ,j  ))*0.5_WP*dyi
           mxtl = -(VOFavg(i  ,j+1)+VOFavg(i  ,j  )-VOFavg(i-1,j+1)-VOFavg(i-1,j  ))*0.5_WP*dxi
           mytl = -(VOFavg(i  ,j+1)-VOFavg(i  ,j  )+VOFavg(i-1,j+1)-VOFavg(i-1,j  ))*0.5_WP*dyi
           mxbl = -(VOFavg(i+1,j  )+VOFavg(i+1,j-1)-VOFavg(i  ,j  )-VOFavg(i  ,j-1))*0.5_WP*dxi
           mybl = -(VOFavg(i+1,j  )-VOFavg(i+1,j-1)+VOFavg(i  ,j  )-VOFavg(i  ,j-1))*0.5_WP*dyi
           mxbr = -(VOFavg(i  ,j  )+VOFavg(i  ,j-1)-VOFavg(i-1,j  )-VOFavg(i-1,j-1))*0.5_WP*dxi
           mybr = -(VOFavg(i  ,j  )-VOFavg(i  ,j-1)+VOFavg(i-1,j  )-VOFavg(i-1,j-1))*0.5_WP*dyi
           mx   = 0.25_WP*(mxtr + mxtl + mxbl + mxbr)
           my   = 0.25_WP*(mytr + mytl + mybl + mybr)
           m    = sqrt(mx**2.0_WP + my**2.0_WP)
           VOFnorm(i,j,1) = mx/(m+epsilon(1.0_WP))
           VOFnorm(i,j,2) = my/(m+epsilon(1.0_WP))
        end if
     end do
  end do
  
  ! Domain boundaries - Neumann
  if (iproc.eq.1  ) VOFnorm(imin_-1,:,:) = VOFnorm(imin_,:,:)
  if (iproc.eq.npx) VOFnorm(imax_+1,:,:) = VOFnorm(imax_,:,:)
  if (jproc.eq.1  ) VOFnorm(:,jmin_-1,:) = VOFnorm(:,jmin_,:)
  if (jproc.eq.npy) VOFnorm(:,jmax_+1,:) = VOFnorm(:,jmax_,:)
  ! Processor boundaries
  call communication_border(VOFnorm(imin_-1:imax_+1,jmin_-1:jmax_+1,1))
  call communication_border(VOFnorm(imin_-1:imax_+1,jmin_-1:jmax_+1,2))
  
  return
end subroutine vof_normal

! ========================= !
!  SLIC Flux Determination  !
! ========================= !
subroutine vof_flux
  use variables
  use grid
  use vof
  implicit none
  integer :: i,j
  real(WP) :: CF
  real(WP) :: VOFadx,VOFady
  
  do j = jmin_,jmax_
     do i = imin_,imax_
        
        ! Choose donor VOF
        VOFadx = VOFavg(i,j)
        VOFady = VOFavg(i,j)
        
        ! Choose acceptor VOF if
        ! 1 - Surface is being convected normal to itself
        if (abs(VOFslop(i,j)).lt.1.0_WP .and. v(i,j).gt.u(i,j))   VOFadx = VOFavg(i+1,j)
        if (abs(VOFslop(i,j)).lt.1.0_WP .and. u(i,j).gt.v(i,j))   VOFady = VOFavg(i,j+1)
        if (abs(VOFslop(i,j)).gt.1.0_WP .and. u(i,j).gt.v(i,j))   VOFadx = VOFavg(i+1,j)
        if (abs(VOFslop(i,j)).gt.1.0_WP .and. v(i,j).gt.u(i,j))   VOFady = VOFavg(i,j+1)
        ! 2 - Acceptor cell or cell upstream of donor cell is empty
        if (VOFavg(i+1,j).lt.VOF_lo .or. VOFavg(i-1,j).lt.VOF_lo) VOFadx = VOFavg(i+1,j)
        if (VOFavg(i,j+1).lt.VOF_lo .or. VOFavg(i,j-1).lt.VOF_lo) VOFady = VOFavg(i,j+1)
        
        ! Compute flux
        ! ------------
        ! X-Pass
        CF = max((1.0_WP-VOFadx)*abs(u(i,j)*dt*dy) - (1.0_WP-VOFavg(i,j))*dx*dy,0.0_WP)
        VOFflux(i,j,1) = min((VOFadx*abs(u(i,j)*dt*dy) + CF),(VOFavg(i,j)*dx*dy))
        ! Y-Pass
        CF = max((1.0_WP-VOFady)*abs(v(i,j)*dt*dy) - (1.0_WP-VOFavg(i,j))*dx*dy,0.0_WP)
        VOFflux(i,j,2) = min((VOFady*abs(v(i,j)*dt*dy) + CF),(VOFavg(i,j)*dx*dy))
        
     end do
  end do
  
  return
end subroutine vof_flux

! ============== !
!  Advect fluid  !
! ============== !
subroutine vof_advect
  use variables
  use grid
  use parallel
  use vof
  implicit none
  integer :: ierr
  integer :: i,j,k
  real(WP) :: fluidvolume,vtotal

  ! X-Pass
  do j = jmin_,jmax_
     do i = imin_,imax_
        VOFavg(i,j) = (+VOFflux(i-1,j,1) + VOFavg(i,j)*dx*dy - VOFflux(i,j,1))*dxi*dyi
     end do
  end do
  ! Y-Pass
  do j = jmin_,jmax_
     do i = imin_,imax_
        VOFavg(i,j) = (+VOFflux(i,j-1,2) + VOFavg(i,j)*dx*dy - VOFflux(i,j,2))*dxi*dyi
     end do
  end do
  
  ! fluidvolume = 0.0_WP
  ! do j = jmin_,jmax_
  !    do i = imin_,imax_
  !       fluidvolume = fluidvolume + VOFavg(i,j)*dx*dy
  !    end do
  ! end do
  ! call MPI_ALLREDUCE(fluidvolume,vtotal,1,mpi_double_precision,mpi_sum,comm,ierr)
  ! if (irank.eq.1) print*,'Total Fluid in system = ',vtotal

  return
end subroutine vof_advect

! ========================= !
!  Bookkeeping adjustments  !
! ========================= !
subroutine vof_adjust
  use variables
  use vof
  implicit none
  integer :: i,j
  
  do j = jmin_,jmax_
     do i = imin_,imax_
        ! Min-Max Adjustment
        VOFavg(i,j) = min(max(VOFavg(i,j),0.0_WP),1.0_WP)
        ! Empty Adjustment
        if (abs(VOFavg(i,j)).lt.VOF_lo ) VOFavg(i,j) = 0.0_WP
        ! Full Adjustment
        if (abs(VOFavg(i,j)).gt.VOF_hi) VOFavg(i,j) = 1.0_WP
     end do
  end do
  
  return
end subroutine vof_adjust

! =============================== !
!  Interface Slope Determination  !
! =============================== !
subroutine vof_slope
  use grid
  use vof
  implicit none
  integer :: i,j
  
  VOFslop = 0.0_WP
  do j = jmin_,jmax_
     do i = imin_,imax_
        if ((VOFavg(i+1,j).eq.0.0_WP .or. VOFavg(i-1,j).eq.0.0_WP .or. &
             VOFavg(i,j+1).eq.0.0_WP .or. VOFavg(i,j-1).eq.0.0_WP) .and. &
             VOFavg(i,j).gt.0.0_WP) then
           VOFslop(i,j) = (VOFavg(i+1,j)-VOFavg(i-1,j))/(VOFavg(i,j-1)-VOFavg(i,j+1))
           if (VOFslop(i,j).gt. 20.0_WP) VOFslop(i,j) = 20.0_WP
           if (VOFslop(i,j).lt.-20.0_WP) VOFslop(i,j) = 20.0_WP
        end if
     end do
  end do
  
  return
end subroutine vof_slope

! =============================== !
!  PLIC Interface Reconstruction  !
! =============================== !
subroutine vof_reconstruct
  use grid
  use vof
  implicit none
  integer :: i,j
  real(WP) :: m,VOF1
  
  do j = jmin_,jmax_
     do i = imin_,imax_
        m = VOFnorm(i,j,1)
        VOF1 = m/2*(1.0_WP-m)
        ! Identify if interface exists here
        if (VOFavg(i,j).ge.VOF_lo .and. VOFavg(i,j).lt.VOF_hi) then
           ! From Zaleski (2000)
           if (VOFavg(i,j).gt.VOF_lo .and. VOFavg(i,j).lt.VOF1) &
                VOFalph(i,j) = sqrt(2*m*VOFavg(i,j)*(1.0_WP-m))
           if (VOFavg(i,j).ge.VOF1 .and. VOFavg(i,j).le.0.5_WP) &
                VOFalph(i,j) = VOFavg(i,j)*(1.0_WP-m) + 0.5_WP*m
        end if
     end do
  end do
  
  return
end subroutine vof_reconstruct
