! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ------------------------------- !
!     This module solves the      !
!    Pressure Poisson equation    !
!     using the HYPRE library     !
! ------------------------------- !
!        Available solvers        !
! - PFMG                          !
! - SMG                           !
! - PCG                           !
! - BICGSTAB                      !
! - GMRES                         !
! - HYBRID                        !
! ------------------------------- !

module pressure
  use variables
  use grid
  use parallel
  use communication
  implicit none
  
  ! HYPRE objects
  integer :: dim_hypre
  integer(kind=8) :: hypre_grid
  integer(kind=8) :: stencil
  integer(kind=8) :: matrix
  integer(kind=8) :: rhs
  integer(kind=8) :: sol
  integer(kind=8) :: solver
  integer(kind=8) :: precond
  integer, dimension(2) :: periodicity_hypre
  integer, dimension(2) :: lower,upper
  integer,  dimension(:), allocatable :: sten_ind
  real(WP), dimension(:), allocatable :: val
  integer :: stencil_size

  ! Preconditioner info
  integer :: precond_id,relax_type

end module pressure

! ============================ !
!  Initialize pressure solver  !
! ============================ !
subroutine pressure_init
  use pressure
  implicit none
  
  ! Allocate pressure array
  allocate(p(imino_:imaxo_,jmino_:jmaxo_)); p=0.0_WP

  call pressure_hypre_init
  call pressure_hypre_solver_init
  call pressure_hypre_set_ig

  return
end subroutine pressure_init

! ========================= !
!  Pressure Solver Routine  !
! ========================= !
subroutine pressure_solve
  use pressure
  implicit none

  call pressure_hypre_rhs_transfer
  call pressure_hypre_solve
  call pressure_hypre_sol_transfer

  call communication_border(p(imin_-1:imax_+1,jmin_-1:jmax_+1))
  call boundary_domain_neumann(p)

  return
end subroutine pressure_solve

! ================================= !
!  Initiating HYPRE Poisson Solver  !
! ================================= !
subroutine pressure_hypre_init
  use pressure
  implicit none
  integer :: ierr
  integer :: count
  integer :: offsets(7,2)
  integer, dimension(2) :: offset
  integer, dimension(6) :: matrix_num_ghost

  ! Determine Hypre dimensions
  dim_hypre = 2

  ! Initialize Hypre - Struct
  call HYPRE_StructGridCreate(comm,dim_hypre,hypre_grid,ierr)
  lower = 0; upper = 0
  lower(1) = imin_
  upper(1) = imax_
  lower(2) = jmin_
  upper(2) = jmax_
  call HYPRE_StructGridSetExtents(hypre_grid,lower(1:dim_hypre),upper(1:dim_hypre),ierr)
  periodicity_hypre = 0
  call HYPRE_StructGridSetPeriodic(hypre_grid,periodicity_hypre(1:dim_hypre),ierr)
  call HYPRE_StructGridAssemble(hypre_grid,ierr)

  ! Build the stencil
  stencil_size = 2*dim_hypre+1
  offsets(1,:) = [ 0, 0]
  offsets(2,:) = [-1, 0]
  offsets(3,:) = [ 1, 0]
  offsets(4,:) = [ 0,-1]
  offsets(5,:) = [ 0, 1]
  call HYPRE_StructStencilCreate(dim_hypre,stencil_size,stencil,ierr)
  do count=1,stencil_size
     offset = offsets(count,:)
     call HYPRE_StructStencilSetElement(stencil,count-1,offset,ierr)
  end do

  ! Build the matrix
  call HYPRE_StructMatrixCreate(comm,hypre_grid,stencil,matrix,ierr)
  matrix_num_ghost = 0
  call HYPRE_StructMatrixSetNumGhost(matrix,matrix_num_ghost(1:2*dim_hypre),ierr)
  call HYPRE_StructMatrixInitialize(matrix,ierr)

  ! Prepare RHS
  call HYPRE_StructVectorCreate(comm,hypre_grid,rhs,ierr)
  call HYPRE_StructVectorInitialize(rhs,ierr)
  call HYPRE_StructVectorAssemble(rhs,ierr)

  ! Create solution vector
  call HYPRE_StructVectorCreate(comm,hypre_grid,sol,ierr)
  call HYPRE_StructVectorInitialize(sol,ierr)
  call HYPRE_StructVectorAssemble(sol,ierr)

  ! Create stencil index
  allocate(sten_ind(stencil_size))
  allocate(val(stencil_size))
  do count=1,stencil_size
     sten_ind(count) = count-1
  end do

  return
end subroutine pressure_hypre_init

! ========================= !
!  Initiating HYPRE Solver  !
! ========================= !
subroutine pressure_hypre_solver_init
  use pressure
  implicit none
  integer  :: ierr
  integer  :: log_level
  real(WP) :: conv_tol
  integer  :: dscg_max_iter
  integer  :: pcg_max_iter
  integer  :: solver_type

  ! Update the operator
  call pressure_hypre_update

  ! Setup the preconditioner
  call hypre_precond_init
  
  ! Setup the solver
  log_level = 1

  select case(trim(pressure_solver))
  case('PFMG')
     call HYPRE_StructPFMGCreate(comm,solver,ierr)
     call HYPRE_StructPFMGSetMaxIter(solver,max_piter,ierr)
     call HYPRE_StructPFMGSetTol(solver,tolp,ierr)
     call HYPRE_StructPFMGSetRelaxType(solver,relax_type,ierr)
     call HYPRE_StructPFMGSetLogging(solver,log_level,ierr)
     call HYPRE_StructPFMGSetup(solver,matrix,rhs,sol,ierr)
  case('SMG')
     call HYPRE_StructSMGCreate(comm,solver,ierr)
     call HYPRE_StructSMGSetMaxIter(solver,max_piter,ierr)
     call HYPRE_StructSMGSetTol(solver,tolp,ierr)
     call HYPRE_StructSMGSetLogging(solver,log_level,ierr)
     call HYPRE_StructSMGSetup(solver,matrix,rhs,sol,ierr)
  case('PCG')
     call HYPRE_StructPCGCreate(comm,solver,ierr)
     call HYPRE_StructPCGSetMaxIter(solver,max_piter,ierr)
     call HYPRE_StructPCGSetTol(solver,tolp,ierr)
     call HYPRE_StructPCGSetLogging(solver,log_level,ierr)
     if (precond_id.ge.0) call HYPRE_StructPCGSetPrecond(solver,precond_id,precond,ierr)
     call HYPRE_StructPCGSetup(solver,matrix,rhs,sol,ierr)
  case('BICGSTAB')
     call HYPRE_StructBICGSTABCreate(comm,solver,ierr)
     call HYPRE_StructBICGSTABSetMaxIter(solver,max_piter,ierr)
     call HYPRE_StructBICGSTABSetTol(solver,tolp,ierr)
     call HYPRE_StructBICGSTABSetLogging(solver,log_level,ierr)
     if (precond_id.ge.0) call HYPRE_StructBICGSTABSetPrecond(solver,precond_id,precond,ierr)
     call HYPRE_StructBICGSTABSetup(solver,matrix,rhs,sol,ierr)
  case('GMRES')
     call HYPRE_StructGMRESCreate(comm,solver,ierr)
     call HYPRE_StructGMRESSetMaxIter(solver,max_piter,ierr)
     call HYPRE_StructGMRESSetTol(solver,tolp,ierr)
     call HYPRE_StructGMRESSetLogging(solver,log_level,ierr)
     if (precond_id.ge.0) call HYPRE_StructGMRESSetPrecond(solver,precond_id,precond,ierr)
     call HYPRE_StructGMRESSetup(solver,matrix,rhs,sol,ierr)
  case ('HYBRID')
     call HYPRE_StructHYBRIDCreate(comm,solver,ierr)
     dscg_max_iter=2*max_piter
     call HYPRE_StructHYBRIDSetDSCGMaxIte(solver,dscg_max_iter,ierr)
     pcg_max_iter=max_piter
     call HYPRE_StructHYBRIDSetPCGMaxIter(solver,pcg_max_iter,ierr)
     call HYPRE_StructHYBRIDSetTol(solver,tolp,ierr)
     conv_tol=0.9_WP
     call HYPRE_StructHYBRIDSetConvergenc(solver,conv_tol,ierr)
     call HYPRE_StructHYBRIDSetLogging(solver,log_level,ierr)
     solver_type=1
     call HYPRE_StructHybridSetSolverType(solver,solver_type,ierr)
     if (precond_id.ge.0) call HYPRE_StructHYBRIDSetPrecond(solver,precond_id,precond,ierr)
     call HYPRE_StructHYBRIDSetup(solver,matrix,rhs,sol,ierr)
  end select

  return
end subroutine pressure_hypre_solver_init

! ============================================ !
!  Initialization of the HYPRE preconditioner  !
! ============================================ !
subroutine hypre_precond_init
  use pressure
  implicit none
  
  integer  :: ierr
  integer  :: precond_max_iter
  real(WP) :: precond_cvg
  
  select case(trim(pressure_precond))
  case('PFMG')
     call HYPRE_StructPFMGCreate(comm,precond,ierr)
     precond_max_iter=1
     call HYPRE_StructPFMGSetMaxIter(precond,precond_max_iter,ierr)
     precond_cvg=0.0_WP
     call HYPRE_StructPFMGSetTol(precond,precond_cvg,ierr)
     call HYPRE_StructPFMGSetRelaxType(precond,relax_type,ierr)
     precond_id = 1
  case('SMG')
     call HYPRE_StructSMGCreate(comm,precond,ierr)
     precond_max_iter=1
     call HYPRE_StructSMGSetMaxIter(precond,precond_max_iter,ierr)
     precond_cvg=0.0_WP
     call HYPRE_StructSMGSetTol(precond,precond_cvg,ierr)
     precond_id = 0
  case('DIAGONAL')
     precond = 0
     precond_id = 8
  case ('none')
     precond_id = -1
  case default
     call die('Hypre_precond_init: Unknown HYPRE preconditioner')
  end select
  
  return
end subroutine hypre_precond_init

! ============================= !
!  Set Initial Guess for HYPRE  !
! ============================= !
subroutine pressure_hypre_set_ig
  use pressure
  implicit none
  integer :: i,j,ierr
  integer, dimension(3) :: ind
  real(WP) :: value

  do j=jmin_,jmax_
     do i=imin_,imax_
        ind(1) = i
        ind(2) = j
        value  = p(i,j)
        call HYPRE_StructVectorSetValues(sol,ind(1:dim_hypre),value,ierr)
     end do
  end do
  call HYPRE_StructVectorAssemble(sol,ierr)

  return
end subroutine pressure_hypre_set_ig

! ============== !
!  HYPRE Solver  !
! ============== !
subroutine pressure_hypre_solve
  use pressure
  implicit none
  integer :: ierr
  real(WP) :: max_res
  
  select case(trim(pressure_solver))
  case('PFMG')
     call HYPRE_StructPFMGSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructPFMGGetNumIteration(solver,piter,ierr)
     call HYPRE_StructPFMGGetFinalRelativ(solver,max_res,ierr)
  case('SMG')
     call HYPRE_StructSMGSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructSMGGetNumIterations(solver,piter,ierr)
     call HYPRE_StructSMGGetFinalRelative(solver,max_res,ierr)
  case ('PCG')
     call HYPRE_StructPCGSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructPCGGetNumIterations(solver,piter,ierr)
     call HYPRE_StructPCGGetFinalRelative(solver,max_res,ierr)
  case ('BICGSTAB')
     call HYPRE_StructBICGSTABSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructBICGSTABGetNumItera(solver,piter,ierr)
     call HYPRE_StructBICGSTABGetFinalRel(solver,max_res,ierr)
  case ('GMRES')
     call HYPRE_StructGMRESSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructGMRESGetNumIteratio(solver,piter,ierr)
     call HYPRE_StructGMRESGetFinalRelati(solver,max_res,ierr)
  case ('HYBRID')
     call HYPRE_StructHYBRIDSolve(solver,matrix,rhs,sol,ierr)
     call HYPRE_StructHYBRIDGetNumIterati(solver,piter,ierr)
     call HYPRE_StructHYBRIDGetFinalRelat(solver,max_res,ierr)
  end select

  if (irank.eq.1) then
     ! print *,'No  of iters =',piter
     ! print *,'Max Residual =',max_res
  end if

  return
end subroutine pressure_hypre_solve

! =========================== !
!  Update the HYPRE operator  !
! =========================== !
subroutine pressure_hypre_update
  use boundary
  use pressure
  implicit none
  integer :: i,j,ierr
  integer, dimension(2) :: ind

  do j=jmin_,jmax_
     do i=imin_,imax_
        ind(1) = i
        ind(2) = j
        
        ! ! Matrix coefficients
        ! val    =  0.0_WP
        ! val(1) =  2.0_WP*(dxi**2+dyi**2)
        ! val(2) = -1.0_WP*dxi**2
        ! val(3) = -1.0_WP*dxi**2
        ! val(4) = -1.0_WP*dyi**2
        ! val(5) = -1.0_WP*dyi**2

        ! Matrix coefficients
        val    = 0.0_WP
        val(1) = &
             -dxi**2*((rho(i+1,j)+rho(i,j))**(-1) + (rho(i,j)+rho(i-1,j))**(-1))&
             -dyi**2*((rho(i,j+1)+rho(i,j))**(-1) + (rho(i,j)+rho(i,j-1))**(-1))
        val(2) = +dxi**2*(rho(i,j)+rho(i-1,j))**(-1)
        val(3) = +dxi**2*(rho(i+1,j)+rho(i,j))**(-1)
        val(4) = +dyi**2*(rho(i,j)+rho(i,j-1))**(-1)
        val(5) = +dyi**2*(rho(i,j+1)+rho(i,j))**(-1)

        ! ! Neumann BCs
        ! if (i.eq.imin) then
        !    val(1) = val(1)+val(2)
        !    val(2) = 0.0_WP
        ! end if
        ! if (i.eq.imax) then
        !    val(1) = val(1)+val(3)
        !    val(3) = 0.0_WP
        ! end if
        ! if (j.eq.jmin) then
        !    val(1) = val(1)+val(4)
        !    val(4) = 0.0_WP
        ! end if
        ! if (j.eq.jmax) then
        !    val(1) = val(1)+val(5)
        !    val(5) = 0.0_WP
        ! end if
        
        if (i.eq.imin) then
           if (c_ul.eq.0 .or. c_vl.eq.0) then ! Dirichlet BCs
              val(1) = val(1)-val(2)
              val(2) = 0.0_WP
           else ! Neumann
              val(1) = val(1)+val(2)
              val(2) = 0.0_WP
           end if
        end if
        if (i.eq.imax) then
           if (c_ur.eq.0 .or. c_vr.eq.0) then ! Dirichlet BCs
              val(1) = val(1)-val(3)
              val(3) = 0.0_WP
           else ! Neumann
              val(1) = val(1)+val(3)
              val(3) = 0.0_WP
           end if
        end if
        if (j.eq.jmin) then
           if (c_ub.eq.0 .or. c_vb.eq.0) then ! Dirichlet BCs !!!! CHECK CHECK
              val(1) = val(1)-val(4)
              val(4) = 0.0_WP
           else ! Neumann
              val(1) = val(1)+val(4)
              val(4) = 0.0_WP
           end if
        end if
        if (j.eq.jmax) then
           if (c_ut.eq.0 .or. c_vt.eq.0) then ! Dirichlet BCs !!!! CHECK CHECK
              val(1) = val(1)-val(5)
              val(5) = 0.0_WP
           else ! Neumann
              val(1) = val(1)+val(5)
              val(5) = 0.0_WP
           end if
        end if
        
        ! Print values
        ! if (i.eq.5) print*,'MAT VAL',j,val(4),val(2),val(1),val(3),val(5)

        call HYPRE_StructMatrixSetValues(matrix,ind(1:dim_hypre),stencil_size,sten_ind,val,ierr)
     end do
  end do

  call HYPRE_StructMatrixAssemble(matrix,ierr)
  ! call HYPRE_StructMatrixPrint(matrix,1,ierr)

  return
end subroutine pressure_hypre_update

! ==================== !
!  HYPRE RHS transfer  !
! ==================== !
subroutine pressure_hypre_rhs_transfer
  use pressure
  implicit none
  integer :: i,j,ierr
  integer, dimension(2) :: ind
  real(WP) :: value,dti

  dti = dt**(-1)
  do j = jmin_,jmax_
     do i = imin_,imax_
        ind(1) = i
        ind(2) = j

        ! RHS value
        value = 0.5_WP*dti*((us(i+1,j)-us(i,j))*dxi + (vs(i,j+1)-vs(i,j))*dyi)
        
        ! Set the value
        call HYPRE_StructVectorSetValues(rhs,ind(1:dim_hypre),value,ierr)
        
     end do
  end do

  call HYPRE_StructVectorAssemble(rhs,ierr)
  ! call HYPRE_StructVectorPrint(rhs,0,ierr)

  return
end subroutine pressure_hypre_rhs_transfer

! ========================= !
!  HYPRE Solution transfer  !
! ========================= !
subroutine pressure_hypre_sol_transfer
  use pressure
  implicit none
  integer :: i,j,ierr
  integer, dimension(2) :: ind
  real(WP) :: value

  do j=jmin_,jmax_
     do i=imin_,imax_
        ind(1) = i
        ind(2) = j

        ! Obtain solution at i,j
        call HYPRE_StructVectorGetValues(sol,ind(1:dim_hypre),value,ierr)

        ! Migrate solution into cell
        p(i,j) = value
        
     end do
  end do
  
  return
end subroutine pressure_hypre_sol_transfer
