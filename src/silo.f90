! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ------------------------------- !
!     This module outputs data    !
!       to SILO files to aid      !
!     visualization of results    !
! ------------------------------- !

module silo
  use variables
  use parallel
  use grid
  implicit none
  include 'silo_f9x.inc'

  character(len=50) :: siloname,dirname
  integer :: nGhost
  integer :: imin_out,imax_out,jmin_out,jmax_out
  
  ! Index
  real(WP), dimension(:,:,:), allocatable :: c_i
  
  ! Interface
  logical  :: write_interface
  integer  :: nNodes,nZones
  integer,  dimension(:), allocatable :: nodeList
  real(SP), dimension(:), allocatable :: xNode,yNode,zNode,ind
  integer, dimension(:,:), allocatable :: zoneIndex
  integer :: nZoneAllocated
  real(SP), dimension(:), allocatable :: interface_buf

end module silo

! =============== !
!  Initiate SILO  !
! =============== !
subroutine silo_init
  use silo
  implicit none
  
  if (irank.eq.1) call execute_command_line('rm -fr Visit')
  if (irank.eq.1) call execute_command_line('mkdir -p Visit/; mkdir -p Visit/silo/')
  if (irank.eq.1) open (21, file='Visit/sim.visit')
  
  allocate(c_i(imin_:imax_,jmin_:jmax_,1:2))
  
  nGhost=min(2,nover-1)
  imin_out=imin_-nGhost; imax_out=imax_+nGhost
  jmin_out=jmin_-nGhost; jmax_out=jmax_+nGhost
  
  ! Determine cell indices
  call silo_cellindex

  ! Initial condition output
  call silo_step
  
  return
end subroutine silo_init

! =========== !
!  SILO step  !
! =========== !
subroutine silo_step
  use silo
  implicit none

  ! Prepare data fields for output
  call velocity_interpolation

  ! Generate interface from VOF
  call silo_interface
  
  ! Write to SILO file
  call silo_write

  ! Write multimesh/multivar SILO file
  call silo_multimeshvar

  ! Add silo file entry to .visit file
  call silo_dotvisit
  
  return
end subroutine silo_step

! ==================== !
!  Write to SILO file  !
! ==================== !
subroutine silo_write
  use vof
  use property
  use tension
  use silo
  implicit none
  integer :: dbfile,ierr
  integer :: proccount
  
  ! Create the silo database
  write(siloname,'(A,I0.5,A)') 'Visit/silo/step',step,'.silo'
  if (irank.eq.1) then
     ierr = dbcreate(siloname,len_trim(siloname),DB_CLOBBER,DB_LOCAL,"Silo database",13,DB_HDF5,dbfile)
     if (dbfile.eq.-1) print *,'Could not create Silo file!'
     ierr = dbclose(dbfile)
  end if
  call MPI_BARRIER(comm,ierr)

  ! Each processor writes its data
  do proccount=1,numproc
     if (irank.eq.proccount) then
        ! Open silo file
        ierr = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)
        write(dirname,'(I5.5)') irank
        ierr = dbmkdir(dbfile,dirname,5,ierr)
        ierr = dbsetdir(dbfile,dirname,5)
        
        ! Mesh
        ierr = dbputqm(dbfile,'Mesh',4,'xc',2,'yc',2,'zc',2,x(imin_:imax_+1),y(jmin_:jmax_+1),&
             DB_F77NULL,(/nx_+1,ny_+1/),2,DB_DOUBLE,DB_COLLINEAR,DB_F77NULL,ierr)
        ! uMesh
        ierr = dbputqm(dbfile,'uMesh',5,'xc',2,'yc',2,'zc',2,xm(imin_-1:imax_+1),y(jmin_:jmax_+1),&
             DB_F77NULL,(/nx_+2,ny_+1/),2,DB_DOUBLE,DB_COLLINEAR,DB_F77NULL,ierr)
        ! vMesh
        ierr = dbputqm(dbfile,'vMesh',5,'xc',2,'yc',2,'zc',2,x(imin_:imax_+1),ym(jmin_-1:jmax_+1),&
             DB_F77NULL,(/nx_+1,ny_+2/),2,DB_DOUBLE,DB_COLLINEAR,DB_F77NULL,ierr)
        ! i
        ierr = dbputqv1(dbfile,'i',1,"Mesh",4,real(c_i(:,:,1),SP),(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_FLOAT,DB_ZONECENT,DB_F77NULL,ierr)
        ! j
        ierr = dbputqv1(dbfile,'j',1,"Mesh",4,real(c_i(:,:,2),SP),(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_FLOAT,DB_ZONECENT,DB_F77NULL,ierr)
        ! Ui
        ierr = dbputqv1(dbfile,'Ui',2,"Mesh",4,ui,(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! Vi
        ierr = dbputqv1(dbfile,'Vi',2,"Mesh",4,vi,(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! Us
        ierr = dbputqv1(dbfile,'Us',2,"uMesh",5,us(imin_:imax_+1,jmin_:jmax_),(/nx_+1,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! Vs
        ierr = dbputqv1(dbfile,'Vs',2,"vMesh",5,vs(imin_:imax_,jmin_:jmax_+1),(/nx_,ny_+1/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! P
        ierr = dbputqv1(dbfile,'P',1,"Mesh",4,p(imin_:imax_,jmin_:jmax_),(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! VOF
        ierr = dbputqv1(dbfile,'VOF',3,"Mesh",4,VOFavg(imin_:imax_,jmin_:jmax_),(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! Density
        ierr = dbputqv1(dbfile,'Density',7,"Mesh",4,rho(imin_:imax_,jmin_:jmax_),(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! Viscosity
        ierr = dbputqv1(dbfile,'Viscosity',9,"Mesh",4,mu(imin_:imax_,jmin_:jmax_),(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! Normal_u_x
        ierr = dbputqv1(dbfile,'Normal_u_x',10,"uMesh",5,normal_u(imin_:imax_+1,jmin_:jmax_,1),(/nx_+1,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! Normal_u_y
        ierr = dbputqv1(dbfile,'Normal_u_y',10,"uMesh",5,normal_u(imin_:imax_+1,jmin_:jmax_,2),(/nx_+1,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! Normal_v_x
        ierr = dbputqv1(dbfile,'Normal_v_x',10,"vMesh",5,normal_v(imin_:imax_,jmin_:jmax_+1,1),(/nx_,ny_+1/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! Normal_v_y
        ierr = dbputqv1(dbfile,'Normal_v_y',10,"vMesh",5,normal_v(imin_:imax_,jmin_:jmax_+1,2),(/nx_,ny_+1/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! Curvature
        ierr = dbputqv1(dbfile,'Curvature',9,"Mesh",4,kappa(imin_:imax_,jmin_:jmax_),(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! srcU
        ierr = dbputqv1(dbfile,'srcU',4,"Mesh",4,srcU(imin_:imax_,jmin_:jmax_),(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! srcv
        ierr = dbputqv1(dbfile,'srcV',4,"Mesh",4,srcV(imin_:imax_,jmin_:jmax_),(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)
        ! VOFslope
        ierr = dbputqv1(dbfile,'VOFslope',8,"Mesh",4,VOFslop(imin_:imax_,jmin_:jmax_),(/nx_,ny_/),2,&
             DB_F77NULL,0,DB_DOUBLE,DB_ZONECENT,DB_F77NULL,ierr)

        ! Interface mesh
        ! Write interface as unstructured mesh made of tetrahedra
        ! err = dbputzl2(dbfile,"zonelist",8,nZones,3,nodeList,3*nZones,1,0,0 &
        !      ,DB_ZONETYPE_TRIANGLE,3,nZones,1,DB_F77NULL,ierr)
        ! err = dbputum(dbfile,"Interface",9,3,xNode(1:nNodes),yNode(1:nNodes),zNode(1:nNodes) &
        !      ,"xInt",4,"yInt",4,"zInt",4,DB_FLOAT,nNodes,nZones,"zonelist",8,DB_F77NULL,0,DB_F77NULL,ierr)
        ! deallocate(xNode,yNode,zNode,nodeList)

        ierr = dbclose(dbfile)
     end if
     call MPI_BARRIER(comm,ierr)
  end do

  return
end subroutine silo_write

! ========================== !
!  Write multimesh/multivar  !
! ========================== !
subroutine silo_multimeshvar
  use vof
  use silo
  implicit none
  integer :: dbfile,err,ierr
  integer :: proccount
  integer,dimension(numproc) :: lnames,types
  character(len=20),dimension(numproc) :: names

  if (irank.eq.1) then
     ierr = dbopen(siloname,len_trim(siloname),DB_HDF5,DB_APPEND,dbfile)
     ierr = dbset2dstrlen(len(names))

     ! Mesh
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Mesh'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUAD_RECT
     end do
     ierr = dbputmmesh(dbfile,"Mesh",4,numproc,names,lnames,types,DB_F77NULL,ierr)

     ! uMesh
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/uMesh'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUAD_RECT
     end do
     ierr = dbputmmesh(dbfile,"uMesh",5,numproc,names,lnames,types,DB_F77NULL,ierr)

     ! vMesh
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/vMesh'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUAD_RECT
     end do
     ierr = dbputmmesh(dbfile,"vMesh",5,numproc,names,lnames,types,DB_F77NULL,ierr)

     ! i
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/i'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"i",1,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! j
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/j'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"j",1,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! Index vector
     err = dbputdefvars(dbfile,'Index',5,1,'Index',5,DB_VARTYPE_VECTOR,'{i,j}',5,DB_F77NULL,ierr)

     ! Ui
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Ui'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Ui",2,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! Vi
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Vi'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Vi",2,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! Velocity vector
     err = dbputdefvars(dbfile,'Velocity',8,1,'Velocity',8,DB_VARTYPE_VECTOR,'{Ui,Vi}',7,DB_F77NULL,ierr)

     ! Us
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Us'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Us",2,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! Vs
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Vs'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Vs",2,numproc,names,lnames,types,DB_F77NULL,ierr)

     ! P
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/P'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"P",1,numproc,names,lnames,types,DB_F77NULL,ierr)

     ! VOF
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/VOF'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"VOF",3,numproc,names,lnames,types,DB_F77NULL,ierr)

     ! Density
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Density'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Density",7,numproc,names,lnames,types,DB_F77NULL,ierr)

     ! Viscosity
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Viscosity'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Viscosity",9,numproc,names,lnames,types,DB_F77NULL,ierr)

     ! Curvature
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Curvature'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Curvature",9,numproc,names,lnames,types,DB_F77NULL,ierr)

     ! Normal_u_x
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Normal_u_x'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Normal_u_x",10,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! Normal_u_y
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Normal_u_y'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Normal_u_y",10,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! Normal_u vector
     err = dbputdefvars(dbfile,'Normal_u',8,1,'Normal_u',8,DB_VARTYPE_VECTOR,'{Normal_u_x,Normal_u_y}',23,DB_F77NULL,ierr)

     ! Normal_v_x
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Normal_v_x'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Normal_v_x",10,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! Normal_v_y
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/Normal_v_y'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"Normal_v_y",10,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! Normal_v vector
     err = dbputdefvars(dbfile,'Normal_v',8,1,'Normal_v',8,DB_VARTYPE_VECTOR,'{Normal_v_x,Normal_v_y}',23,DB_F77NULL,ierr)

     ! VOFslope
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/VOFslope'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"VOFslope",8,numproc,names,lnames,types,DB_F77NULL,ierr)

     ! srcU
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/srcU'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"srcU",4,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! srcV
     do proccount=1,numproc
        write(names(proccount),'(I5.5,A)') proccount,'/srcV'
        lnames(proccount)=len_trim(names(proccount))
        types(proccount)=DB_QUADVAR
     end do
     ierr = dbputmvar(dbfile,"srcV",4,numproc,names,lnames,types,DB_F77NULL,ierr)
     ! src vector
     err = dbputdefvars(dbfile,'src',3,1,'src',3,DB_VARTYPE_VECTOR,'{srcU,srcV}',11,DB_F77NULL,ierr)

     ! ! Interface
     ! do proccount=1,numproc
     !    write(names(i),'(A,I5.5,A,I5.5,A)') 'group',group_ids(i),'.silo:',proc_nums(i),'/Interface'
     !    lnames(i)=len_trim(names(i))
     !    types(i)=DB_UCDMESH
     ! end do
     ! ierr = dbmkoptlist(4,optlist)
     ! ierr = dbaddiopt(optlist, DBOPT_DTIME, time);
     ! ierr = dbaddiopt(optlist, DBOPT_CYCLE, nout_time);
     ! ierr = dbaddiopt(optlist, DBOPT_EXTENTS_SIZE, 6)
     ! ierr = dbadddopt(optlist, DBOPT_EXTENTS, spatial_extents)
     ! ierr = dbputmmesh(dbfile, "Interface", 9, nproc, names,lnames, types, optlist, ierr)
     ! ierr = dbfreeoptlist(optlist)

     ierr = dbclose(dbfile)
  end if

  return
end subroutine silo_multimeshvar

! ====================== !
!  Write to .visit file  !
! ====================== !
subroutine silo_dotvisit
  use silo
  implicit none

  if (irank.eq.1) open (21, file='Visit/sim.visit',position = 'append')
  if (irank.eq.1) write(21,'(A,I0.5,A)') 'silo/step',step,'.silo'
  if (irank.eq.1) close(21)

  return
end subroutine silo_dotvisit

! ============================ !
!  Create cell index for mesh  !
! ============================ !
subroutine silo_cellindex
  use silo
  implicit none
  integer :: i,j
  
  do j = jmin_,jmax_
     do i = imin_,imax_
        c_i(i,j,1) = real(i,WP);
        c_i(i,j,2) = real(j,WP)
     end do
  end do
  
  return
end subroutine silo_cellindex

! ====================================== !
! Creates unstructured mesh on interface !
! ====================================== !
subroutine silo_interface
  use vof
  implicit none

  ! NGA
  ! ---
  
  ! integer :: i,j,ii,jj
  ! integer :: c,cc,ntet,n,nn,nnn,dim,ntri
  ! integer :: case,case2,v1,v2
  ! integer :: nAllocated,nListAllocated
  ! real(SP), dimension(:), allocatable :: tmpNode
  ! integer,  dimension(:), allocatable :: tmpNodeList
  ! integer,  dimension(:,:), allocatable :: tmpZoneIndex
  ! real(WP), parameter :: VOFlo_visit=0.00001
  ! real(WP), parameter :: VOFhi_visit=0.99999
  ! real(WP), dimension(2,4) :: verts
  ! real(WP), dimension(2,4) :: tverts,tverts2
  ! real(WP), dimension(3,4) :: mytet
  ! real(WP), dimension(4) :: d
  ! real(WP), dimension(2) :: node,cen
  ! real(WP) :: mu
  ! real(WP) :: sdx2,sdy2,sdz2

  ! ! Initialize arrays
  ! nAllocated     = 1000
  ! nListAllocated = 1000
  ! allocate(xNode   (nAllocated))
  ! allocate(yNode   (nAllocated))
  ! allocate(zNode   (nAllocated))
  ! allocate(nodeList(nListAllocated))
  ! ! Allocate buffers for interface variables
  ! if (.not.allocated(zoneIndex)) then
  !    nZoneAllocated = 1000
  !    allocate(zoneIndex(3,nZoneAllocated))
  ! end if
  ! nNodes=0
  ! nZones=0
  
  ! do j=jmin_,jmax_
  !    do i=imin_,imax_
        
  !       ! Cell center
  !       cen=(/xm(i),ym(j)/)
        
  !       ! Cells with interface
  !       if (VOFavg(i,j).ge.VOFlo_visit.and.VOFavg(i,j).le.VOFhi_visit) then
           
  !          ! Cell vertices
  !          verts(:,1)=(/x(i  ),y(j  )/)
  !          verts(:,2)=(/x(i+1),y(j  )/)
  !          verts(:,3)=(/x(i  ),y(j+1)/)
  !          verts(:,4)=(/x(i+1),y(j+1)/)
           
  !          ! ! Calculate distance from each vertex to cut plane
  !          ! do n=1,4
  !          !    d(n) = normx(c,i,j,k)*mytet(1,n) &
  !          !         + normy(c,i,j,k)*mytet(2,n) &
  !          !         + normz(c,i,j,k)*mytet(3,n) &
  !          !         - dist(c,i,j,k)
  !          ! end do
  !          ! ! Find cut case
  !          ! case=1+int(0.5_WP+sign(0.5_WP,d(1)))+&
  !          !      2*int(0.5_WP+sign(0.5_WP,d(2)))+&
  !          !      4*int(0.5_WP+sign(0.5_WP,d(3)))+&
  !          !      8*int(0.5_WP+sign(0.5_WP,d(4)))
  !          ! ! Add points on cut plane to nodes
  !          ! do n=1,cut_nvert(case)
  !          !    v1=cut_v1(n,case); v2=cut_v2(n,case)
  !          !    mu=min(1.0_WP,max(0.0_WP,-d(v1)/(sign(abs(d(v2)-d(v1))+epsilon(1.0_WP),d(v2)-d(v1)))))
  !          !    node=(1.0_WP-mu)*mytet(:,v1)+mu*mytet(:,v2)
  !          !    nNodes=nNodes+1
  !          !    xNode(nNodes)=real(node(1),SP)
  !          !    yNode(nNodes)=real(node(2),SP)
  !          !    zNode(nNodes)=real(node(3),SP)
  !          ! end do
  !          ! ! Create node list to tris on interface
  !          ! do n=1,cut_ntris(case)
  !          !    do nn=1,3
  !          !       nodeList(3*nZones+nn)=nNodes-cut_nvert(case)+(cut_vtri(nn,n,case)-4)
  !          !    end do
  !          !    nZones=nZones+1
  !          !    zoneIndex(:,nZones)=(/ i,j,k /)
  !          ! end do

  !       end if
        
  !       ! Reallocate arrays if necessary
  !       if (nAllocated-nNodes.lt.200) then
  !          allocate(tmpNode(nAllocated))
  !          tmpNode=xNode; deallocate(xNode); allocate(xNode(nAllocated+1000)); xNode(1:nAllocated)=tmpNode
  !          tmpNode=yNode; deallocate(yNode); allocate(yNode(nAllocated+1000)); yNode(1:nAllocated)=tmpNode
  !          tmpNode=zNode; deallocate(zNode); allocate(zNode(nAllocated+1000)); zNode(1:nAllocated)=tmpNode
  !          deallocate(tmpNode)
  !          nAllocated=nAllocated+1000
  !       end if
  !       if (nListAllocated-3*nZones.lt.200) then
  !          allocate(tmpNodeList(nListAllocated))
  !          tmpNodeList=nodeList; 
  !          deallocate(nodeList); 
  !          allocate(nodeList(nListAllocated+1000)); 
  !          nodeList(1:nListAllocated)=tmpNodeList
  !          deallocate(tmpNodeList)
  !          nListAllocated=nListAllocated+1000
  !       end if
  !       if (nZoneAllocated-nZones.lt.200) then
  !          allocate(tmpZoneIndex(3,nZoneallocated))
  !          tmpZoneIndex=zoneIndex
  !          deallocate(zoneIndex)
  !          allocate(zoneIndex(3,nZoneAllocated+1000))
  !          Zoneindex(:,1:nZoneAllocated)=tmpZoneIndex
  !          deallocate(tmpZoneIndex)
  !          nZoneAllocated=nZoneAllocated+1000
  !       end if
        
  !    end do
  ! end do

  return
end subroutine silo_interface
