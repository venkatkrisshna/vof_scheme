! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ------------------------------- !
!     This module manages the     !
!    various fluid properties     !
! ------------------------------- !

module property
  use grid
  implicit none

end module property

! =========================== !
!  Inititate property arrays  !
! =========================== !
subroutine property_init
  use variables
  use property
  implicit none

  ! Allocate property arrays
  allocate(rho(imino_:imaxo_,jmino_:jmaxo_)); rho=0.0_WP
  allocate(mu (imino_:imaxo_,jmino_:jmaxo_)); mu =0.0_WP

  ! Initialize surface tension model
  call tension_init

  ! Set initial properties
  call property_update
  
  return
end subroutine property_init

! ======================== !
!  Update property arrays  !
! ======================== !
subroutine property_update
  use property
  implicit none
  integer :: i,j
  
  ! Update properties
  do j = jmin_,jmax_
     do i = imin_,imax_
        ! Density averaging
        select case(trim(DENS_met))
        case ('linear'  ); rho(i,j) = VOFavg(i,j)*rho_1  + (1.0_WP-VOFavg(i,j))*rho_2
        case ('harmonic'); rho(i,j) = rho_1 - rho_2*rho_1/(rho_1*VOFavg(i,j) + rho_2*(1.0_WP-VOFavg(i,j)))
        end select
        ! Viscosity averaging
        select case(trim(VISC_met))
        case ('linear'  ); mu(i,j) = VOFavg(i,j)*mu_1  + (1.0_WP-VOFavg(i,j))*mu_2
        case ('harmonic'); mu(i,j) = mu_1 - mu_2*mu_1/(mu_1*VOFavg(i,j) + mu_2*(1.0_WP-VOFavg(i,j)))
        end select
     end do
  end do
  
  ! Update surface tension forces
  call tension_update
  
  ! Update boundaries
  call communication_border(rho(imin_-1:imax_+1,jmin_-1:jmax_+1))
  call boundary_domain_neumann(rho)
  call communication_border(mu (imin_-1:imax_+1,jmin_-1:jmax_+1))
  call boundary_domain_neumann(mu)
  
  return
end subroutine property_update
