! =============================== !
!  2D INCOMPRESSIBLE FLOW SOLVER  !
! =============================== !
!     Author: Venkata Krisshna    !
!      This file is part of a     !
!  2D Incompressible Flow Solver  !
! ------------------------------- !
!       This module populates     !
!       domain and processor      !
!          boundary cells         !
!                                 !
!        ================         !
!        SIMULATION CASES         !
!        ================         !
!                                 !
! ---------- schannel ----------- ! --------->--------
! Left to right 1D channel flow   ! ->              P=0
! with slip walls                 ! ->              P=0           
! Specify: uleft                  ! --------->--------
!                                 ! 
! ---------- nchannel ----------- ! --------//--------
! Left to right 1D channel flow   ! ->              P=0
! with no-slip walls              ! ->              P=0
! Specify: uleft                  ! --------//--------
!                                 !
! ----------- elbow ------------- ! ------- P=0 ------
! Left inflow + top outflow       ! ->               /
! with stationary walls           ! ->               /
! Specify: uleft                  ! --------//--------
!                                 !
! ------------ lid -------------- ! ->->->->->->->->->
! Lid driven flow with the top    ! /                /
! experiencing tangential flow    ! /                /
! with all other walls stationary ! --------//--------
! Specify: utop                   !
!                                 !
! ---------- couette ------------ ! ->->->->->->->->->
! Lid driven flow with the top    ! =                =
! experiencing tangential flow    ! =                =
! and no-slip bottom wall         ! --------//--------
! Specify: utop                   !
!                                 !
! ----------- oscdrop ----------- ! ==================
! Neumann BC on all walls         ! =                =
! Specify: none                   ! =                =
!                                 ! ==================
! ------------------------------- !

module boundary
  use grid
  use parallel
  implicit none

  ! Boundary case for each wall
  integer  :: c_ut,c_ub,c_ul,c_ur,c_vt,c_vb,c_vl,c_vr
  
end module boundary

! ==================================== !
!  Initialize boundary condition cases !
! ==================================== !
subroutine boundary_init
  use variables
  use boundary
  implicit none

  c_ut = 0; c_ub = 0; c_ul = 0; c_ur = 0;
  c_vt = 0; c_vb = 0; c_vl = 0; c_vr = 0;

  ! Boundary cases =>
  ! 0 : Pressure
  ! 1 : Dirichlet velocity
  ! 2 : Neumann   velocity
  
  select case(trim(simulation))
  case('schannel')
     c_ut = 2; c_ub = 2; c_ul = 1; c_ur = 0;
     c_vt = 1; c_vb = 1; c_vl = 1; c_vr = 1;
     vtop = 0.0_WP; vbot = 0.0_WP; vleft = 0.0_WP; vright = 0.0_WP;
  case('nchannel')
     c_ut = 1; c_ub = 1; c_ul = 1; c_ur = 0;
     c_vt = 1; c_vb = 1; c_vl = 1; c_vr = 1;
     utop = 0.0_WP; ubot = 0.0_WP;
     vtop = 0.0_WP; vbot = 0.0_WP; vleft = 0.0_WP; vright = 0.0_WP;
  case('elbow')
     c_ut = 1; c_ub = 1; c_ul = 1; c_ur = 1;
     c_vt = 0; c_vb = 1; c_vl = 1; c_vr = 1;
     utop = 0.0_WP; ubot = 0.0_WP;                 uright = 0.0_WP;
                    vbot = 0.0_WP; vleft = 0.0_WP; vright = 0.0_WP;
  case('lid')
     c_ut = 1; c_ub = 1; c_ul = 1; c_ur = 1;
     c_vt = 1; c_vb = 1; c_vl = 1; c_vr = 1;
                    ubot = 0.0_WP; uleft = 0.0_WP; uright = 0.0_WP;
     vtop = 0.0_WP; vbot = 0.0_WP; vleft = 0.0_WP; vright = 0.0_WP;
  case('couette')
     c_ut = 1; c_ub = 1; c_ul = 2; c_ur = 2;
     c_vt = 1; c_vb = 1; c_vl = 1; c_vr = 1;
                    ubot = 0.0_WP;
     vtop = 0.0_WP; vbot = 0.0_WP; vleft = 0.0_WP; vright = 0.0_WP;
  case('oscdrop')
     c_ut = 1; c_ub = 1; c_ul = 1; c_ur = 1;
     c_vt = 1; c_vb = 1; c_vl = 1; c_vr = 1;
     utop = 0.0_WP; ubot = 0.0_WP; uleft = 0.0_WP; uright = 0.0_WP;
     vtop = 0.0_WP; vbot = 0.0_WP; vleft = 0.0_WP; vright = 0.0_WP;
  end select
  
  return
end subroutine boundary_init

! ===================================================== !
!  Update domain boundary cells with Neumann Conditions !
! ===================================================== !
subroutine boundary_domain_neumann(bound)
  use communication
  use boundary
  implicit none
  real(WP), dimension(imin_-1:imax_+1,jmin_-1:jmax_+1) :: bound
  
  ! Domain boundaries - Neumann
  if (iproc.eq.1)   bound(imin_-1,:) = bound(imin_,:)
  if (iproc.eq.npx) bound(imax_+1,:) = bound(imax_,:)
  if (jproc.eq.1)   bound(:,jmin_-1) = bound(:,jmin_)
  if (jproc.eq.npy) bound(:,jmax_+1) = bound(:,jmax_)

  return
end subroutine boundary_domain_neumann

! ================================================ !
!  Populate velocity ghost cells with declared BC  !
! ================================================ !
subroutine velocity_boundary(u_buf,v_buf)
  use boundary
  implicit none
  real(WP), dimension(imino_:imaxo_+1,jmino_:jmaxo_) :: u_buf
  real(WP), dimension(imino_:imaxo_,jmino_:jmaxo_+1) :: v_buf

  if (jproc.eq.1  ) then ! Bottom wall
     if(c_vb.eq.1)&
          v_buf(:,jmin_) = vbot
     if(c_vb.eq.2 .or. c_vb.eq.0)&
          v_buf(:,jmin_) = v_buf(:,jmin_+1)
     if(c_ub.eq.1)&
          u_buf(:,jmin_-1) = u_buf(:,jmin_) - 2*(u_buf(:,jmin_) - ubot)
     if(c_ub.eq.2 .or. c_ub.eq.0)&
          u_buf(:,jmin_-1) = u_buf(:,jmin_) - 2*(u_buf(:,jmin_) - u_buf(:,jmin_+1))
  end if
  if (jproc.eq.npy) then ! Top wall
     if(c_vt.eq.1)&
          v_buf(:,jmax_+1) = vtop
     if(c_vt.eq.2 .or. c_vt.eq.0)&
          v_buf(:,jmax_+1) = v_buf(:,jmax_)
     if(c_ut.eq.1)&
          u_buf(:,jmax_+1) = u_buf(:,jmax_) - 2*(u_buf(:,jmax_) - utop)
     if(c_ut.eq.2 .or. c_ut.eq.0)&
          u_buf(:,jmax_+1) = u_buf(:,jmax_) - 2*(u_buf(:,jmax_) - u_buf(:,jmax_-1))
  end if
  if (iproc.eq.1  ) then ! Left wall
     if(c_ul.eq.1)&
          u_buf(imin_,:) = uleft
     if(c_ul.eq.2 .or. c_ul.eq.0)&
          u_buf(imin_,:) = u_buf(imin_+1,:)
     if(c_vl.eq.1)&
          v_buf(imin_-1,:) = v_buf(imin_,:) - 2*(v_buf(imin_,:) - vleft)
     if(c_vl.eq.2 .or. c_vl.eq.0)&
          v_buf(imin_-1,:) = v_buf(imin_,:) - 2*(v_buf(imin_,:) - v_buf(imin_+1,:))
  end if
  if (iproc.eq.npx) then ! Right wall
     if(c_ur.eq.1)&
          u_buf(imax_+1,:) = uright
     if(c_ur.eq.2 .or. c_ur.eq.0)&
          u_buf(imax_+1,:) = u_buf(imax_,:)
     if(c_vr.eq.1)&
          v_buf(imax_+1,:) = v_buf(imax_,:) - 2*(v_buf(imax_,:) - vright)
     if(c_vr.eq.2 .or. c_vr.eq.0)&
          v_buf(imax_+1,:) = v_buf(imax_,:) - 2*(v_buf(imax_,:) - v_buf(imax_-1,:))
  end if

  return
end subroutine velocity_boundary
